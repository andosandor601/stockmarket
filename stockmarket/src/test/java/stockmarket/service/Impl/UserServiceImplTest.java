package stockmarket.service.Impl;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;
import org.mindrot.jbcrypt.BCrypt;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import stockmarket.dao.interfaces.JpaUserServiceDao;
import stockmarket.domain.entities.BuyOffer;
import stockmarket.domain.entities.SellOffer;
import stockmarket.domain.entities.Stock;
import stockmarket.domain.entities.User;
import stockmarket.service.impl.UserServiceImpl;
import stockmarket.service.impl.util.CreateOfferRequest;
import stockmarket.service.interfaces.UserService;

public class UserServiceImplTest {

	@Mock
	private JpaUserServiceDao dao;

	private UserService service;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		service = new UserServiceImpl(dao);
	}

	@Test
	public void loginTest() {
		// preparation
		String testUsername = "name";
		String password = "somepasswd";
		User testUser = new User();
		testUser.setName(testUsername);
		testUser.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
		when(dao.findById(eq(User.class), eq(testUsername))).thenReturn(testUser);
		when(dao.update(any())).thenReturn(testUser);

		// tesztelendo method hivasa
		User loggedInUser = service.login(testUsername, password);

		// kiertekeles
		assertEquals(testUsername, loggedInUser.getName());
		verify(dao, times(1)).findById(User.class, "name");
		verify(dao, times(0)).save(any());
	}
	
	@Test
	public void signupTest() {
		// preparation
		String testUsername = "name";
		String password = "somepasswd";
		User testUser = new User();
		testUser.setName(testUsername);
		testUser.setPassword(password);
		testUser.setEmail("email");
		testUser.setBankaccount("01-01");
		when(dao.save(any())).thenReturn(testUser);

		// tesztelendo method hivasa
		User savedUser = service.signup(testUsername, password, "email", "01-01");

		// kiertekeles
		assertEquals(testUsername, savedUser.getName());
		verify(dao, times(1)).save(any());
	}
	
	@Test
	public void uploadMoneyTest(){
		User testUser = new User();
		testUser.setName("name");
		testUser.setPassword("pwd");
		testUser.setEmail("email");
		testUser.setBankaccount("01-01");
		testUser.setMoney(BigDecimal.TEN);
		when(dao.update(any())).thenReturn(testUser);
		testUser.setMoney(BigDecimal.ZERO);
		
		User updatedUser = service.depositMoney(testUser, BigDecimal.TEN);
		
		assertTrue("nem az elvárt a user számláján a pénz", updatedUser.getMoney().equals(BigDecimal.TEN));
		verify(dao, times(1)).update(any());
	}
	
	@Test
	public void transferMoneyTest(){
		User testUser = new User();
		testUser.setName("name");
		testUser.setPassword("pwd");
		testUser.setEmail("email");
		testUser.setBankaccount("01-01");
		testUser.setMoney(BigDecimal.ZERO);
		testUser.setAlloffersvalue(BigDecimal.ZERO);
		testUser.setBuyoffers(new HashSet<>());
		when(dao.update(any())).thenReturn(testUser);
		testUser.setMoney(BigDecimal.TEN);
		
		User updatedUser = service.withdrawMoney(testUser, BigDecimal.TEN);
		
		assertTrue("nem az elvárt a user számláján a pénz", updatedUser.getMoney().equals(BigDecimal.ZERO));
		verify(dao, times(1)).update(any());
	}
	
	@Test
	public void newBuyOfferTest(){
		User testUser = new User();
		testUser.setName("name");
		testUser.setMoney(BigDecimal.TEN);
		testUser.setAlloffersvalue(BigDecimal.ZERO);
		testUser.setBuyoffers(new HashSet<>());
		Stock s = new Stock();
		s.setChart(null);
		s.setName("milka");
		s.setCname("GMBH");
		BuyOffer bo = new BuyOffer();
		bo.setUser(testUser);
		bo.setPrice(BigDecimal.ONE);
		bo.setQuantity(1);
		bo.setStock(s);
		CreateOfferRequest or = new CreateOfferRequest(testUser, 1, BigDecimal.ONE, s);
		when(dao.save(any())).thenReturn(bo);
		
		BuyOffer savedBuyOffer = service.newBuyOffer(or);
		
		assertEquals(testUser.getName(), savedBuyOffer.getUser().getName());
		verify(dao, times(1)).update(any());
		verify(dao, times(1)).save(any());
	}
	
	@Test
	public void newSellOfferTest(){
		User testUser = new User();
		testUser.setName("name");
		testUser.setMoney(BigDecimal.TEN);
		testUser.setAlloffersvalue(BigDecimal.ZERO);
		testUser.setSelloffers(new HashSet<>());
		testUser.setOwnedstocks(new HashMap<>());
		Stock s = new Stock();
		s.setChart(null);
		s.setName("milka");
		s.setCname("GMBH");
		SellOffer bo = new SellOffer();
		bo.setUser(testUser);
		bo.setPrice(BigDecimal.ONE);
		bo.setQuantity(1);
		bo.setStock(s);
		testUser.getOwnedstocks().put(s, 100);
		CreateOfferRequest or = new CreateOfferRequest(testUser, 1, BigDecimal.ONE, s);
		when(dao.save(any())).thenReturn(bo);
		
		SellOffer savedSellOffer = service.newSellOffer(or);
		
		assertEquals(testUser.getName(), savedSellOffer.getUser().getName());
		verify(dao, times(1)).update(any());
		verify(dao, times(1)).save(any());
	}
	

}
