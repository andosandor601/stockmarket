package stockmarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import stockmarket.JpaConfig;
import stockmarket.service.background.InitDatabase;

@SpringBootApplication
@EnableScheduling
public class Main {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Main.class, args);
        context.getBean(InitDatabase.class).initStocks();
    }

    private static AnnotationConfigApplicationContext init() {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(JpaConfig.class);
        ctx.register(StockMarketConfig.class);
        ctx.scan("stockmarket");
        ctx.refresh();
        return ctx;
    }
}
