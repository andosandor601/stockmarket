package stockmarket.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import stockmarket.dao.interfaces.JpaCoreServiceDao;
import stockmarket.domain.entities.BuyOffer;
import stockmarket.domain.entities.Chart;
import stockmarket.domain.entities.SellOffer;
import stockmarket.domain.entities.Stock;
import stockmarket.domain.entities.StockMarket;

@Primary
@Repository
public class JpaCoreServiceDaoImpl implements JpaCoreServiceDao {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public <E> boolean delete(E entity) {
        try {
            this.entityManager.remove(entity);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public <E, K> E findById(Class<E> entityClass, K id) {
        return entityManager.find(entityClass, id);
    }

    @Override
    public <E> E save(E entity) {
        try {
            this.entityManager.persist(entity);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return entity;
    }

    @Override
    public <E> E update(E entity) {
        try {
            this.entityManager.merge(entity);
        } catch (Exception e) {
        }
        return entity;
    }

    @Override
    public <E> List<E> findAll(Class<E> e) {
        return entityManager.createQuery("from " + e.getName(), e).getResultList();
    }

    @Override
    public StockMarket findStockMarketByStock(Stock stock) {
        List<StockMarket> result = entityManager.createQuery("FROM StockMarket s WHERE :stock MEMBER OF s.stocks)", StockMarket.class)
                .setParameter("stock", stock).getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public Set<BuyOffer> findBuyOffersByStock(Stock stock) {
        List<BuyOffer> result = entityManager.createQuery("FROM BuyOffer s WHERE :stock MEMBER OF s.stock)", BuyOffer.class).setParameter("stock", stock)
                .getResultList();
        return result.isEmpty() ? null : new HashSet<BuyOffer>(result);
    }

    @Override
    public Set<SellOffer> findSellOffersByStock(Stock stock) {
        List<SellOffer> result = entityManager.createQuery("FROM SellOffer s WHERE :stock MEMBER OF s.stock)", SellOffer.class).setParameter("stock", stock)
                .getResultList();
        return result.isEmpty() ? null : new HashSet<SellOffer>(result);
    }

    @Override
    public List<Chart> findChartByStock(Stock stock) {
        List<Chart> result = new ArrayList<Chart>(
                entityManager.createQuery("FROM Chart c WHERE c.stock=:stock", Chart.class).setParameter("stock", stock).getResultList());
        return result.isEmpty() ? null : result;
    }

    @Override
    public String findLastChartDateByStock(Stock stock) {
        List<String> result = entityManager.createQuery("SELECT MAX(c.time) FROM Chart c WHERE :stock MEMBER OF c.stock GROUP BY c.time)", String.class)
                .setParameter("stock", stock).getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

}
