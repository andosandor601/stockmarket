package stockmarket.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import stockmarket.dao.interfaces.JpaUserServiceDao;
import stockmarket.domain.entities.Chart;
import stockmarket.domain.entities.Stock;
import stockmarket.domain.entities.StockMarket;
import stockmarket.domain.entities.User;

@Primary
@Repository
public class JpaUserServiceDaoImpl implements JpaUserServiceDao {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public User findUserByUsernameAndPassword(String username, String password) {
        List<User> result = new ArrayList<>();
        try {
            result = entityManager.createQuery("FROM User WHERE name=:name AND password=:password", User.class).setParameter("name", username)
                    .setParameter("password", password).getResultList();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public List<Chart> findChartByStock(Stock stock) {
        List<Chart> result = new ArrayList<Chart>(
                entityManager.createQuery("FROM Chart c WHERE c.stock=:id", Chart.class).setParameter("id", stock.getName()).getResultList());
        return result.isEmpty() ? null : result;
    }

    @Override
    public <E> boolean delete(E entity) {
        try {
            this.entityManager.remove(entity);
        } catch (Exception e) {
            return false;
        }
        return true;

    }

    @Override
    public <E, K> E findById(Class<E> entityClass, K id) {
        return entityManager.find(entityClass, id);
    }

    @Override
    public <E> E save(E entity) {
        try {
            entityManager.persist(entity);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return entity;
    }

    @Override
    public <E> E update(E entity) {
        try {
            this.entityManager.merge(entity);
            return entity;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public <E> List<E> findAll(Class<E> e) {
        return entityManager.createQuery("FROM " + e.getName(), e).getResultList();
    }

    @Override
    public Stock findStockByNameAndCname(String name, String cname) {
        List<Stock> result = entityManager.createQuery("FROM Stock WHERE name=:name AND cname=:cname", Stock.class).setParameter("name", name)
                .setParameter("cname", cname).getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public StockMarket findStockMarketByStock(Stock stock) {
        List<StockMarket> result = entityManager.createQuery("FROM StockMarket s WHERE :stock MEMBER OF s.stocks)", StockMarket.class)
                .setParameter("stock", stock).getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public String findLastChartDateByStock(Stock stock) {
        List<String> result = entityManager.createQuery("SELECT MAX(c.time) FROM Chart c WHERE :stock MEMBER OF c.stock GROUP BY c.time)", String.class)
                .setParameter("stock", stock).getResultList();
        return result.isEmpty() ? null : result.get(0);
    }
}
