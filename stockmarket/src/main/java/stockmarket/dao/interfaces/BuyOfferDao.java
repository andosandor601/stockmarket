package stockmarket.dao.interfaces;

import java.util.Set;

import stockmarket.domain.entities.BuyOffer;
import stockmarket.domain.entities.Stock;

/**
 * BuyOffer Data Access Object Interface
 * 
 * @author Andó Sándor Zsolt
 *
 */
public interface BuyOfferDao {

    /**
     * BuyOfferek megkeresése részvény alapján
     * 
     * @param stock:
     *            a részvény, ami alapján keresni akarunk
     * @return: Set<BuyOffer> a megtalált BuyOfferek
     */
    Set<BuyOffer> findBuyOffersByStock(Stock stock);
}
