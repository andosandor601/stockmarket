package stockmarket.dao.interfaces;

import java.util.List;

import org.springframework.stereotype.Repository;

/**
 * Generikus Dao interfész
 * 
 * @author Andó Sándor Zsolt
 *
 * @param <K>:
 *            A kulcs típusa
 * @param <E>:
 *            Az entitás
 */

@Repository
public interface GenericDao {

    /**
     * Egy entitás törlése
     * 
     * @param entity:
     *            törölni kívánt entitás
     * @return: boolean: sikerült-é vagy nem
     */
    <E> boolean delete(E entity);

    /**
     * Egy entitás keresése id alapján
     * 
     * @param id:
     *            az entitás id-je
     * @return: E: a megtalált entitás
     */
    <E, K> E findById(Class<E> e, K id);

    /**
     * Entitások lekérdezése az adatbázisból
     * 
     * @param e:
     *            Az entitás osztálya
     * @return: List<> az entitások
     */
    <E> List<E> findAll(Class<E> e);

    /**
     * Egy entitás elmentése az adatbázisba
     * 
     * @param entity:
     *            a menteni kívánt entitás
     * @return: E: az elmentett entitás
     */
    <E> E save(E entity);

    /**
     * Egy módosított entitás mentése az adatbásisba
     * 
     * @param entity:
     *            a módosított entitás
     * @return: a módosított entitás
     */
    <E> E update(E entity);

}
