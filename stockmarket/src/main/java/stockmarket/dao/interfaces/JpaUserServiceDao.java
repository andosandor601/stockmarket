package stockmarket.dao.interfaces;

import org.springframework.stereotype.Repository;

/**
 * UserServicehez tartozó jpadaointerfész, ami implementálja a genericdao, userdao, chartdao, stockdao és a stockmarketdao interface-t
 * 
 * @author Andó Sándor Zsolt
 *
 */
@Repository
public interface JpaUserServiceDao extends GenericDao, UserDao, ChartDao, StockDao, StockMarketDao {

}
