package stockmarket.dao.interfaces;

import java.util.List;

import org.springframework.stereotype.Repository;

import stockmarket.domain.entities.Chart;
import stockmarket.domain.entities.Stock;

/**
 * Chart Data Access Object Interface
 * 
 * @author Andó Sándor Zsolt
 *
 */

@Repository
public interface ChartDao {

    /**
     * Chart betöltése részvény alapján
     * 
     * @param stock:
     *            részvény
     * @return Chart: találat
     */
    List<Chart> findChartByStock(Stock stock);

    /**
     * Legutolsó adat lekérdezése részvény alapján
     * 
     * @param stock:
     *            részvény
     * @return String: találat, ha van
     */
    String findLastChartDateByStock(Stock stock);

}
