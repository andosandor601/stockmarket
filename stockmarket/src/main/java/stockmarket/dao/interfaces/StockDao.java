package stockmarket.dao.interfaces;

import org.springframework.stereotype.Repository;

import stockmarket.domain.entities.Stock;

/**
 * Stock Data Access Object Interface
 * 
 * @author Andó Sándor Zsolt
 *
 */
@Repository
public interface StockDao {
    /**
     * Részvény keresése név és cégnév alapján
     * 
     * @param name:
     *            részvény neve
     * @param cname:
     *            kibocsétó neve
     * @return Stock: a talált részvény
     */
    Stock findStockByNameAndCname(String name, String cname);
}
