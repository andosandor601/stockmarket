package stockmarket.dao.interfaces;

import stockmarket.domain.entities.Stock;
import stockmarket.domain.entities.StockMarket;

/**
 * StockMarket Data Access Object Interface
 * 
 * @author Andó Sándor Zsolt
 *
 */
public interface StockMarketDao {

    /**
     * StockMarket megkeresése részvény alapján
     * 
     * @param stock:
     *            a részvény, ami alapján keresünk
     * @return: StockMarket, a találat
     */
    StockMarket findStockMarketByStock(Stock stock);
}
