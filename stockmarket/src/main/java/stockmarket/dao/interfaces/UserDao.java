package stockmarket.dao.interfaces;

import org.springframework.stereotype.Repository;

import stockmarket.domain.entities.User;

/**
 * User Data Access Object Interface
 * 
 * @author Andó Sándor Zsolt
 *
 */

@Repository
public interface UserDao {

    /**
     * felhasználó keresése név+jelszó alapján
     * 
     * @param username:felhnev
     * @param password:jelszó
     * @return User:talált felhasználó
     */
    User findUserByUsernameAndPassword(String username, String password);

}
