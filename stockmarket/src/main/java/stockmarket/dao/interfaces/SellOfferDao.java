package stockmarket.dao.interfaces;

import java.util.Set;

import stockmarket.domain.entities.SellOffer;
import stockmarket.domain.entities.Stock;

/**
 * SellOffer Data Access Object Interface
 * 
 * @author Andó Sándor Zsolt
 *
 */
public interface SellOfferDao {
    /**
     * Megkeresi az adott részvényhez tartozó SellOffereket
     * 
     * @param stock:
     *            A részvény, ami alapján keresünk
     * @return: Set<SellOffer>, A talált SellOfferek
     */
    Set<SellOffer> findSellOffersByStock(Stock stock);
}
