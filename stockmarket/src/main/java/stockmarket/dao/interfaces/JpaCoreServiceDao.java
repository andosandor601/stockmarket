package stockmarket.dao.interfaces;

import org.springframework.stereotype.Repository;

/**
 * CoreServicehez tartozó jpadaointerfész, ami implementálja a genericdao, stockmarketdao, buyofferdao, sellofferdao, chartdao interface-t
 * 
 * @author Andó Sándor Zsolt
 *
 */
@Repository
public interface JpaCoreServiceDao extends GenericDao, StockMarketDao, BuyOfferDao, SellOfferDao, ChartDao {

}
