package stockmarket.web.view;

import java.math.BigDecimal;

public class BigDecimalToStringConverter {

    public static String convertToString(BigDecimal bigDecValue) {
        String stringValue = bigDecValue.stripTrailingZeros().doubleValue() + "";
        stringValue = stringValue.charAt(stringValue.length() - 1) == '.' ? stringValue.substring(0, stringValue.length() - 1) : stringValue.replace('.', ',');
        return stringValue;
    }
}
