package stockmarket.web.view;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import stockmarket.domain.entities.User;
import stockmarket.presenter.UserPresenter;
import stockmarket.web.view.util.exception.LoginValidationException;
import stockmarket.web.view.util.exception.SignupValidationException;
import stockmarket.web.view.util.factory.VaadinComponentFactory;
import stockmarket.web.view.util.validator.LoginViewValidator;

@SpringView(name = "login", ui = MyUI.class)
public class LoginView extends CustomComponent implements View, ViewProvider {
    @Autowired
    UserPresenter presenter;

    private GridLayout mainLayout;
    private GridLayout loginLayout;
    private GridLayout signupLayout;
    private VerticalLayout page;
    private TabSheet tabMenu;

    private Button signupBtn;
    private Button loginBtn;

    private TextField loginUserNameTf;
    private TextField signupUserNameTf;
    private TextField emailTf;
    private TextField bankAccTf;
    private PasswordField loginPasswordTf;
    private PasswordField signupPasswordTf;

    private final String BT_STYLE_NAME = "batton2";
    private final String LAYOUT_STYLE_NAME = "layout";
    private final String BANK_ACC_REGEXP = "^[0-9]{8}([ -]?[0-9]{8}){1,2}$";

    public LoginView() {
        super();
    }

    @Override
    public void enter(ViewChangeEvent event) {
        UI.getCurrent().getPage().setTitle("Bejelentkezés a kereskedéshez");

        buttonInit();
        loginTextFieldInit();
        signupTextFieldInit();
        page = new VerticalLayout();
        mainLayout = new GridLayout(1, 1);
        loginLayout = new GridLayout(3, 3);
        signupLayout = new GridLayout(3, 6);
        tabMenu = new TabSheet();

        mainLayout.addStyleName(LAYOUT_STYLE_NAME);
        mainLayout.addStyleName("shadow");
        loginLayout.addStyleName(LAYOUT_STYLE_NAME);
        signupLayout.addStyleName(LAYOUT_STYLE_NAME);
        tabMenu.setStyleName(ValoTheme.TABSHEET_EQUAL_WIDTH_TABS);

        loginLayout.addComponent(loginBtn, 1, 2);
        loginLayout.addComponent(loginUserNameTf, 1, 0);
        loginLayout.addComponent(loginPasswordTf, 1, 1);

        signupLayout.addComponent(signupBtn, 1, 5);
        signupLayout.addComponent(signupUserNameTf, 1, 1);
        signupLayout.addComponent(signupPasswordTf, 1, 2);
        signupLayout.addComponent(emailTf, 1, 3);
        signupLayout.addComponent(bankAccTf, 1, 4);

        tabMenu.addTab(loginLayout, "Bejelentkezés");
        tabMenu.addTab(signupLayout, "Regisztráció");

        mainLayout.addComponent(tabMenu);
        page.addComponent(mainLayout);

        mainLayout.setRowExpandRatio(0, 1);
        mainLayout.setRowExpandRatio(1, 6);
        loginLayout.setColumnExpandRatio(0, 1);
        loginLayout.setColumnExpandRatio(1, 2);
        loginLayout.setColumnExpandRatio(2, 1);
        loginLayout.setRowExpandRatio(0, 1);
        loginLayout.setRowExpandRatio(1, 1);
        loginLayout.setRowExpandRatio(2, 3);

        signupLayout.setColumnExpandRatio(0, 1);
        signupLayout.setColumnExpandRatio(1, 2);
        signupLayout.setColumnExpandRatio(2, 1);
        signupLayout.setRowExpandRatio(0, 2);
        signupLayout.setRowExpandRatio(1, 1);
        signupLayout.setRowExpandRatio(2, 1);
        signupLayout.setRowExpandRatio(3, 1);
        signupLayout.setRowExpandRatio(4, 1);
        signupLayout.setRowExpandRatio(5, 5);

        mainLayout.setWidth("500px");
        mainLayout.setHeight("550px");
        loginLayout.setSizeFull();
        signupLayout.setSizeFull();

        tabMenu.setSizeFull();
        page.setSizeFull();

        loginLayout.setComponentAlignment(loginPasswordTf, Alignment.MIDDLE_CENTER);
        loginLayout.setComponentAlignment(loginUserNameTf, Alignment.BOTTOM_CENTER);
        loginLayout.setComponentAlignment(loginBtn, Alignment.MIDDLE_CENTER);

        signupLayout.setComponentAlignment(signupPasswordTf, Alignment.MIDDLE_CENTER);
        signupLayout.setComponentAlignment(emailTf, Alignment.MIDDLE_CENTER);
        signupLayout.setComponentAlignment(bankAccTf, Alignment.MIDDLE_CENTER);
        signupLayout.setComponentAlignment(signupUserNameTf, Alignment.MIDDLE_CENTER);
        signupLayout.setComponentAlignment(signupBtn, Alignment.MIDDLE_CENTER);

        page.setComponentAlignment(mainLayout, Alignment.MIDDLE_CENTER);
        UI.getCurrent().setContent(page);
    }

    private void loginTextFieldInit() {
        loginUserNameTf = new TextField();
        loginPasswordTf = new PasswordField();

        loginPasswordTf.setInputPrompt("Jelszó");
        loginUserNameTf.setInputPrompt("Felhasználói név");

        loginUserNameTf.setWidth("100%");
        loginPasswordTf.setWidth("100%");

        loginUserNameTf.setStyleName("usernametextfield");
        loginPasswordTf.setStyleName("passwordtextfield");
    }

    private void signupTextFieldInit() {

        signupUserNameTf = VaadinComponentFactory.generateTextField("Felhasználói név", "usernametextfield");
        emailTf = VaadinComponentFactory.generateTextField("E-mail cím", "mailtextfield");
        bankAccTf = VaadinComponentFactory.generateTextField("Bankszámlaszám", "bankacctextfield");

        signupPasswordTf = new PasswordField();
        signupPasswordTf.setInputPrompt("Jelszó");
        signupPasswordTf.setWidth("100%");
        signupPasswordTf.setStyleName("passwordtextfield");

        signupPasswordTf.addValidator(new StringLengthValidator("nem jó jelszóhossz(min. 8, max 20 karakter)", 8, 20, false));
        emailTf.addValidator(new EmailValidator("hibás e-mail cím"));
        bankAccTf.addValidator(
                new RegexpValidator(BANK_ACC_REGEXP, "nem jó bankszámlaszám(helyes formátum: 2*8 vagy 3*8 számjegy kötőjellel, vagy szóközzel elválasztva )"));
        signupPasswordTf.setValidationVisible(false);
        signupUserNameTf.setValidationVisible(false);
        bankAccTf.setValidationVisible(false);
        emailTf.setValidationVisible(false);
    }

    private void buttonInit() {
        signupBtn = VaadinComponentFactory.generateDefaultButton("Regisztrálás", BT_STYLE_NAME);
        loginBtn = VaadinComponentFactory.generateDefaultButton("Belépés", BT_STYLE_NAME);

        loginBtn.setWidth("100%");
        signupBtn.setWidth("100%");

        loginBtn.addClickListener(this::doLogin);
        signupBtn.addClickListener(this::doSignup);
    }

    private void doLogin(ClickEvent event) {
        try {
            LoginViewValidator.validateLogin(loginUserNameTf, loginPasswordTf);
            User user = presenter.doLogin(loginUserNameTf.getValue(), loginPasswordTf.getValue());
            navigateToInsideAfterAuthentication(user);
        } catch (LoginValidationException e) {
            doNotification(e.getMessage(), Type.ERROR_MESSAGE);
        } catch (Exception e) {
            doNotification(e.getMessage(), Type.ERROR_MESSAGE);
        }
    }

    private void doSignup(ClickEvent event) {
        try {
            LoginViewValidator.validateSignup(signupUserNameTf, emailTf, bankAccTf, signupPasswordTf);
            User user = presenter.doSignup(signupUserNameTf.getValue(), signupPasswordTf.getValue(), emailTf.getValue(), bankAccTf.getValue());
            navigateToInsideAfterAuthentication(user);
        } catch (SignupValidationException e) {
            doNotification(e.getMessage(), Type.ERROR_MESSAGE);
        } catch (Exception e1) {
            doNotification("Ilyen felhasználó már regisztrált", Type.ERROR_MESSAGE);
        }

    }

    private void navigateToInsideAfterAuthentication(User user) {
        if (user != null) {
            ((MyUI) UI.getCurrent()).setUser(user);
            UI.getCurrent().getNavigator().navigateTo("inside");
        }
    }

    private void doNotification(String message, Type type) {
        Notification notif = new Notification(message, type);
        notif.setDelayMsec(500);
        notif.setPosition(Position.MIDDLE_CENTER);
        notif.show(Page.getCurrent());
    }

    @Override
    public String getViewName(String viewAndParameters) {
        return "login";
    }

    @Override
    public View getView(String viewName) {
        return this;
    }
}
