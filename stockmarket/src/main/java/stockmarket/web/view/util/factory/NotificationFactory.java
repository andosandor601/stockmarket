package stockmarket.web.view.util.factory;

import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

public class NotificationFactory {

    public static void generateNotification(String message, Type type) {
        Notification notif = new Notification(message, type);
        notif.setDelayMsec(500);
        notif.setPosition(Position.MIDDLE_CENTER);
        notif.show(Page.getCurrent());
    }
}
