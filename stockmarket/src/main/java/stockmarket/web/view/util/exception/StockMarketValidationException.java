package stockmarket.web.view.util.exception;

public class StockMarketValidationException extends RuntimeException {

    public StockMarketValidationException() {
        super();
    }

    public StockMarketValidationException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }

    public StockMarketValidationException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public StockMarketValidationException(String arg0) {
        super(arg0);
    }

    public StockMarketValidationException(Throwable arg0) {
        super(arg0);
    }
}
