package stockmarket.web.view.util.exception;

public class LoginValidationException extends RuntimeException {
    public LoginValidationException() {
        super();
    }

    public LoginValidationException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }

    public LoginValidationException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public LoginValidationException(String arg0) {
        super(arg0);
    }

    public LoginValidationException(Throwable arg0) {
        super(arg0);
    }
}
