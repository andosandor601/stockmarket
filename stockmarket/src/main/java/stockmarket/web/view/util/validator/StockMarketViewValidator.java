package stockmarket.web.view.util.validator;

import com.vaadin.ui.TextField;
import com.vaadin.ui.ComboBox;

import stockmarket.web.view.util.exception.StockMarketValidationException;

public class StockMarketViewValidator {

    public static void validateManageBalanceFields(TextField amount) {
        if (!amount.isValid() || amount.isEmpty()) {
            throw new StockMarketValidationException("Hibás, vagy rossz adatok");
        }
    }

    public static void validateNewOffer(ComboBox stockComboB) {
        if (!stockComboB.isValid()) {
            throw new StockMarketValidationException("Hiányzó vagy hibásan kitöltött mezők");
        }
    }

    public static void validateNewOfferInputQuantity(Integer inputValue, int stockQuantity) {
        if (inputValue > stockQuantity) {
            throw new StockMarketValidationException("Ennyi részvényt nem tudsz eladni, maximális mennyiség ebből a részvényből: " + stockQuantity);
        }
    }

    public static void validateModifyStopLimitOfferComponents(ComboBox offerIdComboBox, TextField priceTf, TextField quantityTf, TextField activationPriceTf) {
        if (!offerIdComboBox.isValid() || !priceTf.isValid() || !quantityTf.isValid() || !activationPriceTf.isValid()) {
            throw new StockMarketValidationException("Üres, vagy rossz adatok");
        }

    }

    public static void validateModifyNonStopLimitOfferComponents(ComboBox offerIdComboBox, TextField priceTf, TextField quantityTf) {
        if (!offerIdComboBox.isValid() || !priceTf.isValid() || !quantityTf.isValid()) {
            throw new StockMarketValidationException("Üres, vagy rossz adatok");
        }
    }

    public static void validateDeleteOfferComponents(ComboBox offerIdComboBox) {
        if (!offerIdComboBox.isValid()) {
            throw new StockMarketValidationException("Nincs kiválasztva üzletazonosító");
        }
    }

    public static void validateNewStopLimitOfferInputFields(ComboBox stockComboB, TextField setPriceTf, TextField setQuantityTf,
            TextField setActivationPriceTf) {
        if ((!stockComboB.isValid() || !setPriceTf.isValid() || !setQuantityTf.isValid() || !setActivationPriceTf.isValid())) {
            throw new StockMarketValidationException("Hiányzó vagy hibásan kitöltött mezők");
        }
    }

    public static void validateNewNonStopLimitOfferInputFields(ComboBox stockComboB, TextField setPriceTf, TextField setQuantityTf) {
        if ((!stockComboB.isValid() || !setPriceTf.isValid() || !setQuantityTf.isValid())) {
            throw new StockMarketValidationException("Hiányzó vagy hibásan kitöltött mezők");
        }
    }

}
