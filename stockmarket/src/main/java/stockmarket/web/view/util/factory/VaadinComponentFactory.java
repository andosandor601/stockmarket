package stockmarket.web.view.util.factory;

import java.util.List;
import java.util.Map;

import com.vaadin.data.Validator;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

import stockmarket.domain.entities.Stock;
import stockmarket.web.view.component.window.ContentWindow;

public class VaadinComponentFactory {

    private final static String BT_STYLE_NAME = "batton";
    private final static String LABEL_STYLE_NAME = "label";
    private final static String TF_STYLE_NAME = "textfield";

    public static Button generateDefaultButton(String caption, String stylename) {
        Button button = new Button(caption);
        button.addStyleName(stylename);
        return button;
    }

    public static TextField generateTextField(String inputPrompt, String stylename) {
        TextField tf = new TextField();
        tf.setInputPrompt(inputPrompt);
        tf.setWidth("100%");
        tf.setStyleName(stylename);
        tf.addValidator(new NullValidator("üresen maradt a mező", false));
        return tf;
    }

    public static Button generateZoomButton(String caption) {
        Button btn = new Button(caption);
        btn.setSizeFull();
        return btn;
    }

    public static Label generateLabel(String text) {
        Label label = new Label(text);
        label.addStyleName(LABEL_STYLE_NAME);
        label.setSizeUndefined();
        return label;
    }

    public static Button generateMenuButton(String caption) {
        Button btn = new Button(caption);
        btn.setStyleName(BT_STYLE_NAME);
        btn.setWidth("100%");
        return btn;
    }

    public static Button generateButtonWithIcon(String caption, String url) {
        Button btn = new Button(caption);
        btn.setIcon(new ThemeResource(url));
        btn.addStyleName("batton2");
        return btn;
    }
    
    public static Window generateWindow(String caption, String width, String height, boolean isModal) {
        return new ContentWindow(caption, width, height, isModal);
    }
    
    public static TextField generateTextFieldWithConverterAndValidator(boolean isSetNullRepresent, String inputPrompt, boolean isDisabled, boolean isRightAligned,
            Converter<String, ?> converter, Validator validator) {
        TextField tf = new TextField();
        if (isSetNullRepresent) {
            tf.setNullRepresentation("");
        }
        if (isRightAligned) {
            tf.addStyleName("align-right");
        }
        tf.setEnabled(!isDisabled);
        tf.setInputPrompt(inputPrompt);
        tf.addStyleName(TF_STYLE_NAME);
        tf.setConverter(converter);
        tf.addValidator(validator);
        tf.setValidationVisible(false);
        return tf;
    }
    
    public static ComboBox generateComboBoxWithStocks(List<Stock> stocks){
        ComboBox stockSearchComboB = new ComboBox();
        stockSearchComboB.setInputPrompt("Részvény keresése");
        populateComboBoxWithStocks(stocks, stockSearchComboB);
        return stockSearchComboB;
    }

    private static void populateComboBoxWithStocks(List<Stock> stocks, ComboBox stockSearchComboB) {
        for (Stock stock : stocks) {
            stockSearchComboB.addItem(stock.getName() + "-" + stock.getCname());
        }
    }
    
    public static ComboBox generateComboBoxWithUserStocks(Map<Stock, Integer> stocks) {
        ComboBox stockSearchComboB = new ComboBox();
        stockSearchComboB.setWidth("250px");
        stockSearchComboB.setInputPrompt("Részvény keresése név alapján");
        populateComboBoxWithUserStocks(stocks, stockSearchComboB);
        return stockSearchComboB;
    }

    private static void populateComboBoxWithUserStocks(Map<Stock, Integer> stocks, ComboBox stockSearchComboB) {
        for (Map.Entry<Stock, Integer> it : stocks.entrySet()) {
            stockSearchComboB.addItem(it.getKey().getName() + "-" + it.getKey().getCname());
        }
    }

}
