package stockmarket.web.view.util.validator;

import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;

import stockmarket.web.view.util.exception.LoginValidationException;
import stockmarket.web.view.util.exception.SignupValidationException;

public class LoginViewValidator {

    public static void validateSignup(TextField signupUserNameTf, TextField emailTf, TextField bankAccTf, PasswordField signupPasswordTf)
            throws SignupValidationException {
        if (!signupUserNameTf.isValid()) {
            throw new SignupValidationException("Nincs megadva felhasználói név");
        }
        if (!emailTf.isValid()) {
            throw new SignupValidationException("Hibás e-mail cím");
        }
        if (!bankAccTf.isValid()) {
            throw new SignupValidationException("Nem jó bankszámlaszám(helyes formátum: 2*8 vagy 3*8 számjegy kötőjellel, vagy szóközzel elválasztva )");
        }
        if (!signupPasswordTf.isValid()) {
            throw new SignupValidationException("A jelszó minimum 8 karakter hosszú");
        }
    }

    public static void validateLogin(TextField loginUserNameTf, PasswordField loginPasswordTf) throws LoginValidationException {
        if (loginUserNameTf.isEmpty() || loginPasswordTf.isEmpty()) {
            throw new LoginValidationException("Üresen hagyott mező");
        }
    }
}
