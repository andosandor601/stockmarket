package stockmarket.web.view.util.exception;

public class SignupValidationException extends RuntimeException {

    public SignupValidationException() {
        super();
    }

    public SignupValidationException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }

    public SignupValidationException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public SignupValidationException(String arg0) {
        super(arg0);
    }

    public SignupValidationException(Throwable arg0) {
        super(arg0);
    }
}
