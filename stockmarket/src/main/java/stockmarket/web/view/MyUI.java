package stockmarket.web.view;

import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.UI;

import stockmarket.domain.entities.User;

@SpringUI
@Widgetset("VAADIN.widgetsets.stockmarket.web.widgetset.MyAppWidgetset")
@PreserveOnRefresh
@Theme("mytheme")
public class MyUI extends UI {

    @Autowired
    private SpringViewProvider viewProvider;

    private User user;

    Navigator navigator;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setSizeFull();
        getPage().setTitle("Online kereskedés");
        navigator = new Navigator(this, this);
        navigator.addProvider(viewProvider);
        navigator.navigateTo("login");
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false, widgetset = "VAADIN.widgetsets.stockmarket.web.widgetset.MyAppWidgetset")
    public static class MyUIServlet extends VaadinServlet {
    }

}
