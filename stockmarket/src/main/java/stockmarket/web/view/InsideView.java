package stockmarket.web.view;

import stockmarket.domain.entities.Log;
import stockmarket.domain.entities.Stock;
import stockmarket.domain.entities.User;
import stockmarket.presenter.StockTradingPresenter;
import stockmarket.presenter.util.ChartDataRepresenter;
import stockmarket.web.enums.ChartInterval;
import stockmarket.web.enums.WindowType;
import stockmarket.web.view.component.grid.LogUserTransactionsGrid;
import stockmarket.web.view.component.grid.OwnedStocksGrid;
import stockmarket.web.view.component.grid.UserDataGrid;
import stockmarket.web.view.component.grid.UserOffersGrid;
import stockmarket.web.view.component.layout.NewOfferGridLayout;
import stockmarket.web.view.component.window.ManageBalanceWindow;
import stockmarket.web.view.component.window.ModifyOfferWindow;
import stockmarket.web.view.util.factory.NotificationFactory;
import stockmarket.web.view.util.factory.VaadinComponentFactory;
import java.util.List;
import java.util.Map;

import org.dussan.vaadin.dcharts.DCharts;
import org.dussan.vaadin.dcharts.base.elements.XYaxis;
import org.dussan.vaadin.dcharts.base.elements.XYseries;
import org.dussan.vaadin.dcharts.canvasoverlays.HorizontalLine;
import org.dussan.vaadin.dcharts.data.DataSeries;
import org.dussan.vaadin.dcharts.metadata.TooltipAxes;
import org.dussan.vaadin.dcharts.metadata.XYaxes;
import org.dussan.vaadin.dcharts.metadata.locations.TooltipLocations;
import org.dussan.vaadin.dcharts.metadata.renderers.AxisRenderers;
import org.dussan.vaadin.dcharts.options.Axes;
import org.dussan.vaadin.dcharts.options.CanvasOverlay;
import org.dussan.vaadin.dcharts.options.Highlighter;
import org.dussan.vaadin.dcharts.options.Options;
import org.dussan.vaadin.dcharts.options.Series;
import org.dussan.vaadin.dcharts.renderers.series.OhlcRenderer;
import org.dussan.vaadin.dcharts.renderers.tick.AxisTickRenderer;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SpringView(name = "inside", ui = MyUI.class)
public class InsideView extends CustomComponent implements View, ViewProvider {

    @Autowired
    private StockTradingPresenter presenter;

    private String userName;
    private String stockName;
    private double actStockPrice;
    private boolean isCandlestick = true;

    private GridLayout topMenuLayout;
    private VerticalLayout leftMenuLayout;
    private VerticalLayout main;
    private VerticalLayout chartLayout;
    private CssLayout topMenuCssLayout;
    private HorizontalLayout zoomButtonsLayout;
    private HorizontalLayout leftMenuAndContentLayout;
    private HorizontalLayout stockSearchLayout;

    private Panel chartPanel;

    private Label userNameLabel;
    private Label userMoneyLabel;
    private Label actStockNameLabel;
    private Label zoomLabel;

    private Button userDataBtn;
    private Button loginBtn;
    private Button transferMoneyBtn;
    private Button ownedStocksBtn;
    private Button buyOffersBtn;
    private Button sellOffersBtn;
    private Button buyStocksBtn;
    private Button sellStocksBtn;
    private Button logoutBtn;

    private Button zoom1DayBtn;
    private Button zoom1WeekBtn;
    private Button zoom1MonthBtn;

    private Window window;
    private DCharts chart;

    private final String LAYOUT_STYLE_NAME = "layout";

    @Override
    public void enter(ViewChangeEvent event) {
        if (((MyUI) getUI().getCurrent()).getUser() == null) {
            UI.getCurrent().getNavigator().navigateTo("login");
            return;
        }
        UI.getCurrent().getPage().setTitle("Online kereskedés");
        userName = ((MyUI) UI.getCurrent()).getUser().getName();
        main = new VerticalLayout();
        getUI().getCurrent().setContent(main);
        main.setMargin(true);
        main.addStyleName("height-100");

        topMenuCssLayout = new CssLayout();
        main.addComponent(topMenuCssLayout);
        topMenuLayout = new GridLayout(4, 1);

        stockName = "";
        actStockPrice = 0;
        initTopMenuLay();

        topMenuCssLayout.addStyleName(LAYOUT_STYLE_NAME);
        topMenuCssLayout.setHeight("30px");
        topMenuCssLayout.setWidth("100%");
        topMenuCssLayout.addComponent(topMenuLayout);

        leftMenuAndContentLayout = new HorizontalLayout();
        main.addComponent(leftMenuAndContentLayout);

        leftMenuAndContentLayout.setWidth("100%");
        leftMenuAndContentLayout.addStyleName("shadow");
        leftMenuAndContentLayout.addStyleName("menu-and-content");

        leftMenuLayout = new VerticalLayout();
        leftMenuAndContentLayout.addComponent(leftMenuLayout);
        leftMenuLayout.setSizeUndefined();
        leftMenuLayout.addStyleName("menu");
        leftMenuLayout.addStyleName("height-100");

        initMenuButtons();

        chartLayout = new VerticalLayout();
        chartLayout.addStyleName(LAYOUT_STYLE_NAME);

        chartPanel = new Panel(chartLayout);
        chartPanel.addStyleName(LAYOUT_STYLE_NAME);
        chartPanel.setSizeFull();
        leftMenuAndContentLayout.addComponent(chartPanel);
        leftMenuAndContentLayout.setExpandRatio(leftMenuLayout, 1);
        leftMenuAndContentLayout.setExpandRatio(chartPanel, 6);
    }

    private void initZoomButtons() {
        zoomLabel = new Label("Zoom");
        zoomLabel.setStyleName("label");
        zoomLabel.setWidth("100px");

        zoom1DayBtn = VaadinComponentFactory.generateZoomButton(ChartInterval.DAY.toString());
        zoom1WeekBtn = VaadinComponentFactory.generateZoomButton(ChartInterval.WEEK.toString());
        zoom1MonthBtn = VaadinComponentFactory.generateZoomButton(ChartInterval.MONTH.toString());

        zoomButtonsLayout = new HorizontalLayout();
        zoomButtonsLayout.setWidth("400px");
        zoomButtonsLayout.setHeight("25px");
        zoomButtonsLayout.addComponents(zoomLabel, zoom1DayBtn, zoom1WeekBtn, zoom1MonthBtn);
        addZoomButtonsClickListener();
    }

    public void initTopMenuLay() {
        User user = presenter.doFindUser(userName);
        topMenuLayout.removeAllComponents();

        stockSearchLayout = new HorizontalLayout();
        stockSearchLayout.addStyleName(LAYOUT_STYLE_NAME);
        stockSearchLayout.setSizeFull();

        ComboBox stockSearchComboB = initComboBoxFilledWithStocks();
        stockSearchComboB.addStyleName("height-95");

        stockSearchLayout.addComponents(stockSearchComboB);

        stockSearchComboB.addValueChangeListener(e -> {
            doSearchStock(stockSearchComboB);
        });

        actStockNameLabel = stockName.equals("") ? VaadinComponentFactory.generateLabel("")
                : VaadinComponentFactory.generateLabel(stockName + " aktuális ára: " + actStockPrice);

        userNameLabel = VaadinComponentFactory.generateLabel("Üdvözlünk az oldalon " + userName);
        userMoneyLabel = VaadinComponentFactory.generateLabel("Egyenleged: " + BigDecimalToStringConverter.convertToString(user.getMoney()) + " Ft");
        topMenuLayout.addComponents(userNameLabel, userMoneyLabel, stockSearchLayout, actStockNameLabel);
        topMenuLayout.setColumnExpandRatio(0, 1);
        topMenuLayout.setColumnExpandRatio(1, 1);
        topMenuLayout.setColumnExpandRatio(2, 6);
        topMenuLayout.setColumnExpandRatio(3, 2);
        topMenuLayout.setSizeFull();
    }

    private void doSearchStock(ComboBox stockSearchComboB) {
        if (stockSearchComboB.getValue() == null) {
            NotificationFactory.generateNotification("Nincs kiválasztva semmi", Notification.Type.WARNING_MESSAGE);
        } else {
            stockName = stockSearchComboB.getValue().toString();
            initChart(presenter.doLoadChart(stockName, ChartInterval.DAY), stockName);
        }
    }

    private void initMenuButtons() {
        userDataBtn = VaadinComponentFactory.generateMenuButton("Adatlap");
        loginBtn = VaadinComponentFactory.generateMenuButton("Kereskedési történet");
        transferMoneyBtn = VaadinComponentFactory.generateMenuButton("Pénz átutalás");
        ownedStocksBtn = VaadinComponentFactory.generateMenuButton("Saját részvények");
        buyOffersBtn = VaadinComponentFactory.generateMenuButton("Vételi ajánlatok");
        sellOffersBtn = VaadinComponentFactory.generateMenuButton("Eladási ajánlatok");
        buyStocksBtn = VaadinComponentFactory.generateMenuButton("Részvény vásárlás");
        sellStocksBtn = VaadinComponentFactory.generateMenuButton("Részvény eladás");
        logoutBtn = VaadinComponentFactory.generateMenuButton("Kilépés");

        leftMenuLayout.addComponent(userDataBtn);
        leftMenuLayout.addComponent(loginBtn);
        leftMenuLayout.addComponent(transferMoneyBtn);
        leftMenuLayout.addComponent(ownedStocksBtn);
        leftMenuLayout.addComponent(buyOffersBtn);
        leftMenuLayout.addComponent(sellOffersBtn);
        leftMenuLayout.addComponent(buyStocksBtn);
        leftMenuLayout.addComponent(sellStocksBtn);
        leftMenuLayout.addComponent(logoutBtn);
        leftMenuLayout.setSizeFull();

        addMenuButtonsClickListener();
    }

    private void initChart(List<ChartDataRepresenter> cdrlist, String stockData) {
        
        chartLayout.removeAllComponents();
        
        String[] stockDataParts = stockData.trim().split("-");
        Stock stock = presenter.findStock(stockDataParts[0], stockDataParts[1]);
        
        DataSeries dataSeries = new DataSeries();
        dataSeries.newSeries();
        for (ChartDataRepresenter cdrit : cdrlist) {
            dataSeries.add(cdrit.getDate(), cdrit.getOpen(), cdrit.getMax(), cdrit.getMin(), cdrit.getClose());
        }
        actStockPrice = stock.getCurrvalue().doubleValue();
        
        Axes axes = new Axes()
                .addAxis(new XYaxis().setLabel("Időtartam").setShowLabel(false).setRenderer(AxisRenderers.DATE)
                        .setNumberTicks((cdrlist.size() / 4 < 3) ? 3 : cdrlist.size() / 4).setUseSeriesColor(true))
                .addAxis(new XYaxis(XYaxes.Y).setTickOptions(new AxisTickRenderer().setPrefix(" Ár (Ft) ").setTextColor("white")));

        CanvasOverlay canvasOverlay = new CanvasOverlay().setShow(true)
                .setObject(new HorizontalLine().setY(actStockPrice).setLineWidth(2).setColor("rgb(89, 198, 154)").setShadow(false));

        Series series = new Series().addSeries(new XYseries().setRendererOptions(new OhlcRenderer().setCandleStick(isCandlestick).setUpBodyColor("#22B14C")
                .setDownBodyColor("#EE1B24").setFillUpBody(true).setWickColor("grey").setOpenColor("grey").setCloseColor("grey")));

        Highlighter highlighter = new Highlighter().setShow(true).setShowTooltip(true).setTooltipAlwaysVisible(true).setKeepTooltipInsideChart(true)
                .setTooltipLocation(TooltipLocations.NORTH_WEST).setTooltipAxes(TooltipAxes.XY_OHLC);

        Options options = new Options().setAxes(axes).setSeries(series).setHighlighter(highlighter).setCanvasOverlay(canvasOverlay);

        chart = new DCharts().setDataSeries(dataSeries).setOptions(options).show();
        chart.setWidth(((cdrlist.size() * 80 < 900) ? 900 : cdrlist.size() * 80), Unit.PIXELS);
        chart.setHeight((float) (UI.getCurrent().getPage().getBrowserWindowHeight() * 0.75), Unit.PIXELS);
        initZoomButtons();
        initTopMenuLay();
        chartLayout.addComponents(chart, zoomButtonsLayout);
        chartLayout.setComponentAlignment(zoomButtonsLayout, Alignment.MIDDLE_LEFT);
        chartLayout.setExpandRatio(chart, 20);
        chartLayout.setExpandRatio(zoomButtonsLayout, 1);
        chartLayout.setWidth(chart.getWidth() + 40, Unit.PIXELS);
        chartLayout.setHeight((float) (chart.getHeight() * 1.1), Unit.PIXELS);
        leftMenuAndContentLayout.replaceComponent(leftMenuAndContentLayout.getComponent(1), chartPanel);
    }

    private ComboBox initComboBoxFilledWithStocks() {
        List<Stock> stocks = presenter.doListAllStock();
        return VaadinComponentFactory.generateComboBoxWithStocks(stocks);
    }

    private Grid initUserOffersGrid(String text, WindowType windowtype) {
        User user = presenter.doFindUser(userName);
        return new UserOffersGrid(user, text, windowtype);
    }

    private Grid initOwnedStocksGrid() {
        Map<Stock, Integer> ownedStocks = presenter.doFindUser(userName).getOwnedstocks();
        return new OwnedStocksGrid(ownedStocks);
    }

    private Grid initUserDataGrid(String name, String email, String bankAcc, String money, String allOfferValue) {
        return new UserDataGrid(name, email, bankAcc, money, allOfferValue);
    }

    private Grid initLogUserTransactionsGrid() {
        List<Log> userLogs = presenter.doFindUser(userName).getLog();
        return new LogUserTransactionsGrid(userLogs);
    }

    private void addZoomButtonsClickListener() {
        zoom1DayBtn.addClickListener(e -> {
            initChart(presenter.doLoadChart(stockName, ChartInterval.DAY), stockName);
        });
        zoom1WeekBtn.addClickListener(e -> {
            initChart(presenter.doLoadChart(stockName, ChartInterval.WEEK), stockName);
        });
        zoom1MonthBtn.addClickListener(e -> {
            initChart(presenter.doLoadChart(stockName, ChartInterval.MONTH), stockName);
        });
    }

    private void addMenuButtonsClickListener() {
        userDataBtn.addClickListener(this::doListUserData);
        loginBtn.addClickListener(this::doLogUserTransactions);
        transferMoneyBtn.addClickListener(this::doManageBalance);
        ownedStocksBtn.addClickListener(this::doListOwnedStocks);
        buyOffersBtn.addClickListener(this::doListBuyOffers);
        sellOffersBtn.addClickListener(this::doListSellOffers);
        buyStocksBtn.addClickListener(this::doBuyStocks);
        sellStocksBtn.addClickListener(this::doSellStocks);
        logoutBtn.addClickListener(this::doLogout);
    }

    private void doListUserData(ClickEvent e) {
        VerticalLayout userDataLayout = new VerticalLayout();
        initUserDataComponents(userDataLayout);
        userDataLayout.setSizeFull();
        leftMenuAndContentLayout.replaceComponent(leftMenuAndContentLayout.getComponent(1), userDataLayout);
    }

    private void doLogUserTransactions(ClickEvent e) {
        VerticalLayout userDataLayout = new VerticalLayout();
        initLogUserTransactionsComponents(userDataLayout);
        userDataLayout.setSizeFull();
        leftMenuAndContentLayout.replaceComponent(leftMenuAndContentLayout.getComponent(1), userDataLayout);
    }

    private void doManageBalance(ClickEvent event) {
        new ManageBalanceWindow(this, presenter);
    }

    private void doListOwnedStocks(ClickEvent e) {
        VerticalLayout userDataLayout = new VerticalLayout();
        initUserStockComponents(userDataLayout);
        userDataLayout.setSizeFull();
        leftMenuAndContentLayout.replaceComponent(leftMenuAndContentLayout.getComponent(1), userDataLayout);
    }

    private void doListBuyOffers(ClickEvent e) {
        doListBuyOffers();
    }

    public void doListBuyOffers() {
        VerticalLayout userDataLayout = new VerticalLayout();
        initUserOfferComponents(userDataLayout, WindowType.BUY);
        userDataLayout.setSizeFull();
        leftMenuAndContentLayout.replaceComponent(leftMenuAndContentLayout.getComponent(1), userDataLayout);
    }

    private void doListSellOffers(ClickEvent e) {
        doListSellOffers();
    }

    public void doListSellOffers() {
        VerticalLayout userDataLayout = new VerticalLayout();
        initUserOfferComponents(userDataLayout, WindowType.SELL);
        userDataLayout.setSizeFull();
        leftMenuAndContentLayout.replaceComponent(leftMenuAndContentLayout.getComponent(1), userDataLayout);
    }

    private void doBuyStocks(ClickEvent e) {
        closeOpenedWindow();
        window = VaadinComponentFactory.generateWindow("Részvény vásárlása", "700px", "600px", false);
        GridLayout userDataLayout = new NewOfferGridLayout(WindowType.BUY, this, window, presenter);
        window.setContent(userDataLayout);
        UI.getCurrent().addWindow(window);
    }

    private void doSellStocks(ClickEvent e) {
        closeOpenedWindow();
        window = VaadinComponentFactory.generateWindow("Részvények eladása", "700px", "600px", false);
        GridLayout userDataLayout = new NewOfferGridLayout(WindowType.SELL, this, window, presenter);
        window.setContent(userDataLayout);
        UI.getCurrent().addWindow(window);
    }

    private void doLogout(ClickEvent e) {
        closeOpenedWindow();
        ((MyUI) UI.getCurrent()).setUser(null);
        UI.getCurrent().getNavigator().navigateTo("login");
    }

    private void closeOpenedWindow() {
        if (!UI.getCurrent().getWindows().isEmpty()) {
            UI.getCurrent().removeWindow(window);
        }
    }

    public void initUserOfferComponents(VerticalLayout userDataLayout, WindowType windowType) {
        userDataLayout.removeAllComponents();
        String text = WindowType.isBuyOffer(windowType) ? "Vásárlás" : "Eladás";

        Grid grid = initUserOffersGrid(text, windowType);
        Label title = new Label("<h1>" + text + "i ajánlatok </h1>", ContentMode.HTML);

        Button modifyBtn = new Button("Módosítás/Törlés");
        modifyBtn.addStyleName("batton2");

        userDataLayout.setMargin(true);
        userDataLayout.addComponent(title);
        userDataLayout.addComponent(grid);
        userDataLayout.addComponent(modifyBtn);
        userDataLayout.setComponentAlignment(modifyBtn, Alignment.MIDDLE_CENTER);
        userDataLayout.setStyleName(LAYOUT_STYLE_NAME);

        modifyBtn.addClickListener(e -> {
            initModifyOfferWindow(userDataLayout, windowType);
        });
    }

    private void initUserStockComponents(VerticalLayout userDataLayout) {
        userDataLayout.removeAllComponents();
        Grid grid = initOwnedStocksGrid();

        Label title = new Label("<h1>Birtokolt részvények </h1>", ContentMode.HTML);

        userDataLayout.setMargin(true);
        userDataLayout.addComponent(title);
        userDataLayout.addComponent(grid);
        userDataLayout.setStyleName(LAYOUT_STYLE_NAME);
    }

    private void initUserDataComponents(VerticalLayout userDataLayout) {
        User user = presenter.doFindUser(userName);
        userDataLayout.removeAllComponents();

        Grid grid = initUserDataGrid(userName, user.getEmail(), user.getBankaccount(), BigDecimalToStringConverter.convertToString(user.getMoney()) + " Ft",
                BigDecimalToStringConverter.convertToString(user.getAllbuyoffersvalue()) + " Ft");
        Label title = new Label("<h1>Felhasználói adatok </h1>", ContentMode.HTML);

        userDataLayout.setMargin(true);
        userDataLayout.addComponent(title);
        userDataLayout.addComponent(grid);
        userDataLayout.addStyleName(LAYOUT_STYLE_NAME);
    }

    private void initLogUserTransactionsComponents(VerticalLayout userDataLayout) {
        userDataLayout.removeAllComponents();
        Grid grid = initLogUserTransactionsGrid();

        Label title = new Label("<h1>Kereskedési történelem </h1>", ContentMode.HTML);

        userDataLayout.setMargin(true);
        userDataLayout.addComponent(title);
        userDataLayout.addComponent(grid);
        userDataLayout.addStyleName(LAYOUT_STYLE_NAME);
    }

    private void initModifyOfferWindow(VerticalLayout userDataLayout, WindowType windowType) {
        Window window2 = new ModifyOfferWindow(windowType, userDataLayout, this, presenter);
        UI.getCurrent().addWindow(window2);
    }

    @Override
    public String getViewName(String viewAndParameters) {
        return "inside";
    }

    @Override
    public View getView(String viewName) {
        return this;
    }

    public String getUserName() {
        return userName;
    }

}
