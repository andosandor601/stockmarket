package stockmarket.web.view.component.window;

import java.math.BigDecimal;

import com.vaadin.data.util.converter.StringToBigDecimalConverter;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Notification.Type;

import stockmarket.domain.entities.User;
import stockmarket.presenter.StockTradingPresenter;
import stockmarket.web.view.InsideView;
import stockmarket.web.view.util.exception.StockMarketValidationException;
import stockmarket.web.view.util.factory.NotificationFactory;
import stockmarket.web.view.util.factory.VaadinComponentFactory;
import stockmarket.web.view.util.validator.StockMarketViewValidator;

public class ManageBalanceWindow extends Window {

    private final static String LAYOUT_STYLE_NAME = "layout";

    private final User user;
    private final InsideView view;

    private StockTradingPresenter presenter;

    public ManageBalanceWindow(InsideView insideView, StockTradingPresenter presenter) {
        super("Pénzfeltöltés/kivétel");
        this.view = insideView;
        this.user = presenter.doFindUser(view.getUserName());
        this.presenter = presenter;
        this.setWidth("400px");
        this.setHeight("400px");
        this.setModal(true);

        initComponents();
    }

    private void initComponents() {
        TextField amount = VaadinComponentFactory.generateTextFieldWithConverterAndValidator(true, "Összeg megadása", false, true,
                new StringToBigDecimalConverter(), new BigDecimalRangeValidator("nem jók az adatok", BigDecimal.ZERO, new BigDecimal(Math.pow(10, 19))));

        Button addMoneyBtn = VaadinComponentFactory.generateButtonWithIcon("feltöltés", "icons/uploadicon.png");
        Button transferMoneyBtn = VaadinComponentFactory.generateButtonWithIcon("kivétel", "icons/downloadicon.png");

        GridLayout gridLay = new GridLayout(2, 2);
        gridLay.setStyleName(LAYOUT_STYLE_NAME);
        gridLay.addComponent(amount, 0, 0, 1, 0);
        gridLay.addComponents(addMoneyBtn, transferMoneyBtn);
        gridLay.setSizeFull();
        gridLay.setMargin(true);
        gridLay.setComponentAlignment(addMoneyBtn, Alignment.MIDDLE_CENTER);
        gridLay.setComponentAlignment(transferMoneyBtn, Alignment.MIDDLE_CENTER);
        gridLay.setComponentAlignment(amount, Alignment.MIDDLE_CENTER);

        this.setContent(gridLay);
        UI.getCurrent().addWindow(this);

        addMoneyBtn.addClickListener(e -> {
            doAddMoney(amount);
        });

        transferMoneyBtn.addClickListener(e -> {
            doTransferMoney(amount);
        });
    }

    private void doTransferMoney(TextField amount) {
        try {
            StockMarketViewValidator.validateManageBalanceFields(amount);
            presenter.doTransferMoney(user, (BigDecimal) amount.getConvertedValue());
            closeManageBalanceWindow(this);
        } catch (StockMarketValidationException e) {
            NotificationFactory.generateNotification(e.getMessage(), Type.ERROR_MESSAGE);
        } catch (Exception e2) {
            NotificationFactory.generateNotification(e2.getMessage(), Type.ERROR_MESSAGE);
        }
    }

    private void doAddMoney(TextField amount) {
        try {
            StockMarketViewValidator.validateManageBalanceFields(amount);
            presenter.doAddMoney(user, (BigDecimal) amount.getConvertedValue());
            closeManageBalanceWindow(this);
        } catch (StockMarketValidationException e) {
            NotificationFactory.generateNotification(e.getMessage(), Type.ERROR_MESSAGE);
        } catch (Exception e2) {
            NotificationFactory.generateNotification(e2.getMessage(), Type.ERROR_MESSAGE);
        }
    }

    private void closeManageBalanceWindow(Window window2) {
        window2.close();
        view.initTopMenuLay();
    }

}
