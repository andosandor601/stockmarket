package stockmarket.web.view.component.grid;

import com.vaadin.ui.Grid;

public class UserDataGrid extends Grid{

    /**
     * 
     */
    private static final long serialVersionUID = 7996259310879399285L;

    public UserDataGrid(String name, String email, String bankAcc, String money, String allOfferValue) {
        super();
        this.setReadOnly(true);
        this.setSelectionMode(SelectionMode.NONE);
        addColumns();
        addRows(name, email, bankAcc, money, allOfferValue);
        this.setSizeFull();
    }

    private void addRows(String name, String email, String bankAcc, String money, String allOfferValue) {
        this.addRow("Felhasználói név", name);
        this.addRow("Email-cím", email);
        this.addRow("Bankszámlaszám", bankAcc);
        this.addRow("Számlán lévő pénz", money);
        this.addRow("Vásárlási ajánlatok összértéke", allOfferValue);
    }

    private void addColumns() {
        this.addColumn("név", Object.class);
        this.addColumn("adat", Object.class);
    }

}
