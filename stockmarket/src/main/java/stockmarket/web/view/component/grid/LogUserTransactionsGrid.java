package stockmarket.web.view.component.grid;

import java.util.List;

import com.vaadin.ui.Grid;

import stockmarket.domain.entities.Log;
import stockmarket.web.view.BigDecimalToStringConverter;

public class LogUserTransactionsGrid extends Grid {

    /**
     * 
     */
    private static final long serialVersionUID = 7125250676418318806L;

    public LogUserTransactionsGrid(List<Log> userLogs) {
        super();
        this.setReadOnly(true);
        this.setSelectionMode(SelectionMode.NONE);
        addColumns();
        addRows(userLogs);
        this.setSizeFull();
    }

    private void addRows(List<Log> userLogs) {
        userLogs.forEach((log) -> {
            this.addRow(log.getTime().toString(), log.getTransaction_type(), log.getStock_name(), BigDecimalToStringConverter.convertToString(log.getPrice()),
                    log.getQuantity());
        });
    }

    private void addColumns() {
        this.addColumn("Tranzakció dátuma", Object.class);
        this.addColumn("Tranzakció típusa", Object.class);
        this.addColumn("Részvény neve", Object.class);
        this.addColumn("Tranzakció ára", Object.class);
        this.addColumn("Mennyiség", Object.class);
    }

}
