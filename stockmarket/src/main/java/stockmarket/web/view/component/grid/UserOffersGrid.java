package stockmarket.web.view.component.grid;

import java.math.BigDecimal;
import java.util.Set;

import com.vaadin.ui.Grid;

import stockmarket.domain.entities.BuyOffer;
import stockmarket.domain.entities.SellOffer;
import stockmarket.domain.entities.User;
import stockmarket.web.enums.WindowType;
import stockmarket.web.view.BigDecimalToStringConverter;

public class UserOffersGrid extends Grid {

    /**
     * 
     */
    private static final long serialVersionUID = -342942615795378523L;

    public UserOffersGrid(User user, String text, WindowType windowtype) {
        super();
        this.setSelectionMode(SelectionMode.SINGLE);
        addColumns(text);
        addRows(user, windowtype);
        this.setSizeFull();
    }

    private void addRows(User user, WindowType windowtype) {
        if (isBuyOfferWindow(windowtype)) {
            addBuyOffers(user);
        } else {
            addSellOffers(user);
        }
    }

    private void addSellOffers(User user) {
        Set<SellOffer> offers = user.getSelloffers();
        for (SellOffer soit : offers) {
            BigDecimal price = soit.getPrice();
            int quantity = soit.getQuantity();
            String active = soit.isActive() ? "aktív" : "nem aktív";
            String activationPrice = BigDecimalToStringConverter.convertToString(soit.getActivationprice()).equals("0,0") ? "nincs beállítva"
                    : BigDecimalToStringConverter.convertToString(soit.getActivationprice());
            this.addRow(soit.getSellid(), soit.getStock().getName(), soit.getStock().getCname(),
                    BigDecimalToStringConverter.convertToString(soit.getStock().getCurrvalue()), quantity, BigDecimalToStringConverter.convertToString(price),
                    BigDecimalToStringConverter.convertToString(price.multiply(new BigDecimal(quantity))), activationPrice, active);
        }
    }

    private void addBuyOffers(User user) {
        Set<BuyOffer> offers = user.getBuyoffers();
        for (BuyOffer boit : offers) {
            BigDecimal price = boit.getPrice();
            int quantity = boit.getQuantity();
            String active = boit.isActive() ? "aktív" : "nem aktív";
            String activationPrice = BigDecimalToStringConverter.convertToString(boit.getActivationprice()).equals("0,0") ? "nincs beállítva"
                    : BigDecimalToStringConverter.convertToString(boit.getActivationprice());
            this.addRow(boit.getBuyid(), boit.getStock().getName(), boit.getStock().getCname(),
                    BigDecimalToStringConverter.convertToString(boit.getStock().getCurrvalue()), quantity, BigDecimalToStringConverter.convertToString(price),
                    BigDecimalToStringConverter.convertToString(price.multiply(new BigDecimal(quantity))), activationPrice, active);
        }
    }

    private boolean isBuyOfferWindow(WindowType windowtype) {
        return windowtype.equals(WindowType.BUY);
    }

    private void addColumns(String text) {
        this.addColumn(text + " azonosító", Object.class);
        this.addColumn("Részvény neve", Object.class);
        this.addColumn("Részvényt kibocsátó neve", Object.class);
        this.addColumn("Aktuális ár (Ft)", Object.class);
        this.addColumn("Mennyiség (db)", Object.class);
        this.addColumn(text + " ára (Ft)", Object.class);
        this.addColumn("Összesített érték (Ft)", Object.class);
        this.addColumn("StopLimit megbízás", Object.class);
        this.addColumn("Aktív-e a megbízás", Object.class);
    }

}
