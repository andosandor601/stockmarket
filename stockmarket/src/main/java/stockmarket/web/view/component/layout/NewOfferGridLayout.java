package stockmarket.web.view.component.layout;

import java.math.BigDecimal;
import java.util.List;

import com.vaadin.data.util.converter.StringToBigDecimalConverter;
import com.vaadin.data.util.converter.StringToIntegerConverter;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Notification.Type;

import stockmarket.domain.entities.Stock;
import stockmarket.domain.entities.User;
import stockmarket.presenter.StockTradingPresenter;
import stockmarket.web.enums.WindowType;
import stockmarket.web.view.InsideView;
import stockmarket.web.view.util.exception.StockMarketValidationException;
import stockmarket.web.view.util.factory.NotificationFactory;
import stockmarket.web.view.util.factory.VaadinComponentFactory;
import stockmarket.web.view.util.validator.StockMarketViewValidator;

public class NewOfferGridLayout extends GridLayout {

    private final static String LAYOUT_STYLE_NAME = "layout";
    private final InsideView view;
    private final Window window;
    private final WindowType windowType;
    private final User user;
    private StockTradingPresenter presenter;

    private ComboBox stockComboB;
    private Label stockName, price, quantity, activationPrice;
    private CheckBox stopLimitCheckB;
    private TextField setPriceTf, setQuantityTf, setActivationPriceTf;
    private Button buyBtn;

    public NewOfferGridLayout(WindowType windowType, InsideView insideView, Window window, StockTradingPresenter presenter) {
        super(2, 6);
        this.view = insideView;
        this.window = window;
        this.windowType = windowType;
        this.presenter = presenter;
        user = presenter.doFindUser(view.getUserName());
        initNewOfferComponents();

        this.setSizeFull();
    }

    private void initNewOfferComponents() {
        fillComboBoxWithStocks();
        initLabels();
        stopLimitCheckB = new CheckBox("Stop-Limit megbízás", false);
        stockComboB.addValidator(new NullValidator("Nincs kiválasztva részvény", false));
        initTextFields();
        buyBtn = VaadinComponentFactory.generateButtonWithIcon(windowType.getWindowCaption(), "icons/shoppingicon.png");
        setLayout();
        addListenersToComponents();
    }

    private void fillComboBoxWithStocks() {
        if (isBuyOffer(windowType)) {
            stockComboB = initComboBoxFilledWithStocks();
        } else {
            stockComboB = initComboBoxFilledWithUserStocks();
        }
    }

    private void setLayout() {
        this.setMargin(true);
        this.addComponents(stockName, stockComboB, price, setPriceTf, quantity, setQuantityTf);
        this.addComponent(stopLimitCheckB, 0, 3, 1, 3);
        this.addComponents(activationPrice, setActivationPriceTf);
        this.addComponent(buyBtn, 0, 5, 1, 5);

        this.setComponentAlignment(stockName, Alignment.MIDDLE_LEFT);
        this.setComponentAlignment(price, Alignment.MIDDLE_LEFT);
        this.setComponentAlignment(quantity, Alignment.MIDDLE_LEFT);
        this.setComponentAlignment(activationPrice, Alignment.MIDDLE_LEFT);
        this.setComponentAlignment(stopLimitCheckB, Alignment.MIDDLE_LEFT);
        this.setComponentAlignment(buyBtn, Alignment.BOTTOM_CENTER);
        this.setComponentAlignment(stockComboB, Alignment.MIDDLE_CENTER);
        this.setComponentAlignment(setPriceTf, Alignment.MIDDLE_CENTER);
        this.setComponentAlignment(setQuantityTf, Alignment.MIDDLE_CENTER);
        this.setComponentAlignment(setActivationPriceTf, Alignment.MIDDLE_CENTER);

        this.setColumnExpandRatio(0, 1);
        this.setColumnExpandRatio(1, 1);
        this.setRowExpandRatio(0, 1);
        this.setRowExpandRatio(1, 1);
        this.setRowExpandRatio(2, 1);
        this.setRowExpandRatio(3, 1);
        this.setRowExpandRatio(4, 1);
        this.setRowExpandRatio(5, 4);
        this.addStyleName(LAYOUT_STYLE_NAME);
    }

    private void addListenersToComponents() {
        stopLimitCheckB.addValueChangeListener(e -> {
            addValueListenerToStopLimitCheckBox();
        });

        buyBtn.addClickListener(e -> {
            newOffer(stopLimitCheckB.getValue());
        });
    }

    private void initTextFields() {
        setPriceTf = VaadinComponentFactory.generateTextFieldWithConverterAndValidator(true, windowType.getWindowCaption() + " árának megadása", false, true,
                new StringToBigDecimalConverter(), new BigDecimalRangeValidator("nem jók az adatok", BigDecimal.ZERO, null));
        setQuantityTf = VaadinComponentFactory.generateTextFieldWithConverterAndValidator(true, "Mennyiség megadása", false, true,
                new StringToIntegerConverter(), new IntegerRangeValidator("Hibás darabszám", 1, null));
        setActivationPriceTf = VaadinComponentFactory.generateTextFieldWithConverterAndValidator(true, "Stoplimit ár megadása", true, true,
                new StringToBigDecimalConverter(), new BigDecimalRangeValidator("nem jók az adatok", BigDecimal.ZERO, null));
        setPriceTf.addValidator(new NullValidator("Üresen maradt mező", false));
        setQuantityTf.addValidator(new NullValidator("Üresen maradt mező", false));
        setActivationPriceTf.addValidator(new NullValidator("Üresen maradt mező", false));
        setPriceTf.setWidth("250px");
        setQuantityTf.setWidth("250px");
        setActivationPriceTf.setWidth("250px");
    }

    private void initLabels() {
        stockName = VaadinComponentFactory.generateLabel("Részvény neve");
        price = VaadinComponentFactory.generateLabel(windowType.getWindowCaption() + " ára");
        quantity = VaadinComponentFactory.generateLabel(windowType.getWindowCaption() + " mennyisége");
        activationPrice = new Label("Aktivációsár");
        activationPrice.setEnabled(false);
        activationPrice.addStyleName("disabledlabel");
    }

    private void addValueListenerToStopLimitCheckBox() {
        if (stopLimitCheckB.getValue()) {
            setActivationPriceTf.setEnabled(true);
            activationPrice.setEnabled(true);
            activationPrice.setStyleName("label");
        } else {
            setActivationPriceTf.setEnabled(false);
            activationPrice.setEnabled(false);
            activationPrice.setStyleName("disabledlabel");
        }
    }

    private ComboBox initComboBoxFilledWithStocks() {
        List<Stock> stocks = presenter.doListAllStock();
        return VaadinComponentFactory.generateComboBoxWithStocks(stocks);
    }

    private ComboBox initComboBoxFilledWithUserStocks() {
        User user = presenter.doFindUser(view.getUserName());
        return VaadinComponentFactory.generateComboBoxWithUserStocks(user.getOwnedstocks());
    }

    private boolean isBuyOffer(WindowType windowType) {
        return windowType.equals(WindowType.BUY);
    }

    private void newOffer(Boolean isLimitOffer) {
        try {
            StockMarketViewValidator.validateNewOffer(stockComboB);
            String[] stockComboBParts = stockComboB.getValue().toString().trim().split("-");
            placeNewOffer(isLimitOffer, stockComboBParts);
        } catch (StockMarketValidationException e) {
            NotificationFactory.generateNotification(e.getMessage(), Type.ERROR_MESSAGE);
        } catch (Exception e2) {
            NotificationFactory.generateNotification(e2.getMessage(), Type.ERROR_MESSAGE);
        }
    }

    private void placeNewOffer(Boolean isLimitOffer, String[] stockComboBParts) {
        if (isLimitOffer) {
            StockMarketViewValidator.validateNewStopLimitOfferInputFields(stockComboB, setPriceTf, setQuantityTf, setActivationPriceTf);
            placeNewLimitOffer(stockComboBParts);
        } else {
            StockMarketViewValidator.validateNewNonStopLimitOfferInputFields(stockComboB, setPriceTf, setQuantityTf);
            placeNewNonLimitOffer(stockComboBParts);
        }
    }

    private void placeNewNonLimitOffer(String[] stockComboBParts) {
        if (isBuyOffer(windowType)) {
            placeNewBuyOffer();
        } else {
            placeNewSellOffer(stockComboBParts);
        }
    }

    private void placeNewLimitOffer(String[] stockComboBParts) {
        if (isBuyOffer(windowType)) {
            placeNewStopBuyOffer();
        } else {
            placeNewStopSellOffer(stockComboBParts);
        }
    }

    private void placeNewSellOffer(String[] stockComboBParts) {
        int stockQuantity = user.getOwnedstocks().get(presenter.doFindStock(stockComboBParts[0], stockComboBParts[1]));
        StockMarketViewValidator.validateNewOfferInputQuantity((Integer) setQuantityTf.getConvertedValue(), stockQuantity);
        presenter.doNewSellOffer(user, (Integer) setQuantityTf.getConvertedValue(), (BigDecimal) setPriceTf.getConvertedValue(),
                stockComboB.getValue().toString());
        view.doListSellOffers();
        window.close();
    }

    private void placeNewBuyOffer() {
        presenter.doNewBuyOffer(user, (Integer) setQuantityTf.getConvertedValue(), (BigDecimal) setPriceTf.getConvertedValue(),
                stockComboB.getValue().toString());
        view.doListBuyOffers();
        window.close();
    }

    private void placeNewStopSellOffer(String[] stockComboBParts) {
        int stockQuantity = user.getOwnedstocks().get(presenter.doFindStock(stockComboBParts[0], stockComboBParts[1]));
        StockMarketViewValidator.validateNewOfferInputQuantity((Integer) setQuantityTf.getConvertedValue(), stockQuantity);
        presenter.doNewStopSellOffer(user, (Integer) setQuantityTf.getConvertedValue(), (BigDecimal) setPriceTf.getConvertedValue(),
                stockComboB.getValue().toString(), (BigDecimal) setActivationPriceTf.getConvertedValue());
        view.doListSellOffers();
        window.close();
    }

    private void placeNewStopBuyOffer() {
        presenter.doNewStopBuyOffer(user, (Integer) setQuantityTf.getConvertedValue(), (BigDecimal) setPriceTf.getConvertedValue(),
                stockComboB.getValue().toString(), (BigDecimal) setActivationPriceTf.getConvertedValue());
        view.doListBuyOffers();
        window.close();
    }

}
