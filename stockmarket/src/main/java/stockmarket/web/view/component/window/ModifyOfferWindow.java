package stockmarket.web.view.component.window;

import java.math.BigDecimal;

import com.vaadin.data.util.converter.StringToBigDecimalConverter;
import com.vaadin.data.util.converter.StringToIntegerConverter;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Notification.Type;

import stockmarket.domain.entities.BuyOffer;
import stockmarket.domain.entities.SellOffer;
import stockmarket.domain.entities.User;
import stockmarket.presenter.StockTradingPresenter;
import stockmarket.web.enums.WindowType;
import stockmarket.web.view.BigDecimalToStringConverter;
import stockmarket.web.view.InsideView;
import stockmarket.web.view.util.exception.StockMarketValidationException;
import stockmarket.web.view.util.factory.NotificationFactory;
import stockmarket.web.view.util.factory.VaadinComponentFactory;
import stockmarket.web.view.util.validator.StockMarketViewValidator;

public class ModifyOfferWindow extends Window {

    private final static String LAYOUT_STYLE_NAME = "layout";

    private final WindowType windowType;
    private final User user;

    private StockTradingPresenter presenter;

    private ComboBox offerIdComboBox;
    private Button modifyBtn, deleteBtn, cancelBtn;
    private Label offerId, price, quantity, stopLimit, stopLimitPrice;
    private TextField priceTf, quantityTf, activationPriceTf;
    private CheckBox stopLimitCheckB;
    private GridLayout contentLayout;
    private VerticalLayout userDataLayout;
    private final InsideView view;

    public ModifyOfferWindow(WindowType windowType, VerticalLayout userDataLayout, InsideView view, StockTradingPresenter presenter) {
        super(windowType.getWindowCaption() + " ajánlatok módosítása, vagy törlése");
        this.user = presenter.doFindUser(view.getUserName());
        this.windowType = windowType;
        this.userDataLayout = userDataLayout;
        this.view = view;
        this.presenter = presenter;
        this.setResizable(false);
        this.setWidth("1200px");
        this.setHeight("300px");
        this.setModal(true);

        initComponents();
    }

    private void initComponents() {
        initOfferIdComboBox();
        initButtons();
        initTextFields();
        initStopLimitCheckBox();
        initLabels();
        initContentLayout();

        this.setContent(contentLayout);

        addListeners();
    }

    private void addListeners() {
        offerIdComboBox.addValueChangeListener(e -> {
            setOfferComponentValues();
        });

        stopLimitCheckB.addValueChangeListener(e -> {
            if (stopLimitCheckB.getValue()) {
                activationPriceTf.setEnabled(true);
                stopLimitPrice.setEnabled(true);
                stopLimitPrice.setStyleName("label");
            } else {
                activationPriceTf.setEnabled(false);
                stopLimitPrice.setEnabled(false);
                stopLimitPrice.setStyleName("disabledlabel");
            }
        });

        modifyBtn.addClickListener(r -> {
            modifyOffer();
        });

        deleteBtn.addClickListener(r -> {
            deleteOffer();
        });

        cancelBtn.addClickListener(r -> {
            this.close();
        });
    }

    private void initContentLayout() {
        contentLayout = new GridLayout(5, 3);
        contentLayout.addComponents(offerId, price, quantity, stopLimit, stopLimitPrice, offerIdComboBox, priceTf, quantityTf, stopLimitCheckB,
                activationPriceTf);
        contentLayout.addComponent(modifyBtn, 1, 2);
        contentLayout.addComponent(deleteBtn, 2, 2);
        contentLayout.addComponent(cancelBtn, 3, 2);
        contentLayout.setSizeFull();
        contentLayout.setSpacing(true);
        contentLayout.setStyleName(LAYOUT_STYLE_NAME);
        contentLayout.setComponentAlignment(offerId, Alignment.BOTTOM_CENTER);
        contentLayout.setComponentAlignment(price, Alignment.BOTTOM_CENTER);
        contentLayout.setComponentAlignment(quantity, Alignment.BOTTOM_CENTER);
        contentLayout.setComponentAlignment(stopLimit, Alignment.BOTTOM_CENTER);
        contentLayout.setComponentAlignment(stopLimitPrice, Alignment.BOTTOM_CENTER);
        contentLayout.setComponentAlignment(offerIdComboBox, Alignment.MIDDLE_CENTER);
        contentLayout.setComponentAlignment(priceTf, Alignment.MIDDLE_CENTER);
        contentLayout.setComponentAlignment(quantityTf, Alignment.MIDDLE_CENTER);
        contentLayout.setComponentAlignment(stopLimitCheckB, Alignment.MIDDLE_CENTER);
        contentLayout.setComponentAlignment(activationPriceTf, Alignment.MIDDLE_CENTER);
        contentLayout.setComponentAlignment(modifyBtn, Alignment.MIDDLE_CENTER);
        contentLayout.setComponentAlignment(deleteBtn, Alignment.MIDDLE_CENTER);
        contentLayout.setComponentAlignment(cancelBtn, Alignment.MIDDLE_CENTER);
        contentLayout.setRowExpandRatio(0, 1);
        contentLayout.setRowExpandRatio(1, 3);
        contentLayout.setRowExpandRatio(2, 3);
    }

    private void initLabels() {
        offerId = VaadinComponentFactory.generateLabel("Üzlet azonosító");
        price = VaadinComponentFactory.generateLabel("Kereskedás ára");
        quantity = VaadinComponentFactory.generateLabel("Kereskedés mennyisége");
        stopLimit = VaadinComponentFactory.generateLabel("Stoplimit beállítása");
        stopLimitPrice = VaadinComponentFactory.generateLabel("Stoplimit ár meghatározása");
        stopLimitPrice.setStyleName("disabledlabel");
    }

    private void initStopLimitCheckBox() {
        stopLimitCheckB = new CheckBox();
        stopLimitCheckB.setValue(false);
    }

    private void initTextFields() {
        priceTf = VaadinComponentFactory.generateTextFieldWithConverterAndValidator(true, "", false, true, new StringToBigDecimalConverter(),
                new BigDecimalRangeValidator("nem jók az adatok", BigDecimal.ZERO, null));
        quantityTf = VaadinComponentFactory.generateTextFieldWithConverterAndValidator(true, "", false, true, new StringToIntegerConverter(),
                new IntegerRangeValidator("Hibás darabszám", 1, null));
        activationPriceTf = VaadinComponentFactory.generateTextFieldWithConverterAndValidator(true, "", true, true, new StringToBigDecimalConverter(),
                new BigDecimalRangeValidator("nem jók az adatok", BigDecimal.ZERO, null));
    }

    private void initButtons() {
        modifyBtn = VaadinComponentFactory.generateButtonWithIcon("Üzlet módosítása", "icons/editicon.png");
        deleteBtn = VaadinComponentFactory.generateButtonWithIcon("Üzlet törlése", "icons/deleteicon.png");
        cancelBtn = VaadinComponentFactory.generateButtonWithIcon("Mégse", "icons/closeicon.png");
    }

    private void initOfferIdComboBox() {
        offerIdComboBox = new ComboBox();
        offerIdComboBox.addStyleName("align-right");
        offerIdComboBox.setRequired(true);
        offerIdComboBox.setNullSelectionAllowed(false);
        offerIdComboBox.addValidator(new NullValidator("Nincs kiválasztva üzlet azonosító", false));

        fillComboBoxWithOffers();
    }

    private void fillComboBoxWithOffers() {
        if (WindowType.isBuyOffer(windowType)) {
            fillComboBoxWithBuyOffers();
        } else {
            fillComboBoxWithSellOffers();
        }
    }

    private void fillComboBoxWithSellOffers() {
        for (SellOffer soit : user.getSelloffers()) {
            offerIdComboBox.addItem(soit.getSellid());
        }
    }

    private void fillComboBoxWithBuyOffers() {
        for (BuyOffer boit : user.getBuyoffers()) {
            offerIdComboBox.addItem(boit.getBuyid());
        }
    }

    private void setOfferComponentValues() {
        if (WindowType.isBuyOffer(windowType)) {
            setComponentsWithBuyOfferValues();
        } else {
            setComponentsWithSellOfferValues();
        }
    }

    private void setComponentsWithSellOfferValues() {
        priceTf.setValue(BigDecimalToStringConverter.convertToString(presenter.doFindSellOffer((long) offerIdComboBox.getValue()).getPrice()));
        quantityTf.setValue(presenter.doFindSellOffer((long) offerIdComboBox.getValue()).getQuantity() + "");
        stopLimitCheckB.setValue(
                !BigDecimalToStringConverter.convertToString(presenter.doFindSellOffer((long) offerIdComboBox.getValue()).getActivationprice()).equals("0,0"));
        activationPriceTf
                .setValue(BigDecimalToStringConverter.convertToString(presenter.doFindSellOffer((long) offerIdComboBox.getValue()).getActivationprice()));
    }

    private void setComponentsWithBuyOfferValues() {
        priceTf.setValue(BigDecimalToStringConverter.convertToString(presenter.doFindBuyOffer((long) offerIdComboBox.getValue()).getPrice()));
        quantityTf.setValue(presenter.doFindBuyOffer((long) offerIdComboBox.getValue()).getQuantity() + "");
        stopLimitCheckB.setValue(
                !BigDecimalToStringConverter.convertToString(presenter.doFindBuyOffer((long) offerIdComboBox.getValue()).getActivationprice()).equals("0,0"));
        activationPriceTf
                .setValue(BigDecimalToStringConverter.convertToString(presenter.doFindBuyOffer((long) offerIdComboBox.getValue()).getActivationprice()));
    }

    private void modifyOffer() {
        try {
            if (stopLimitCheckB.getValue()) {
                StockMarketViewValidator.validateModifyStopLimitOfferComponents(offerIdComboBox, priceTf, quantityTf, activationPriceTf);
                doModifyOffer((BigDecimal) activationPriceTf.getConvertedValue());
            } else {
                StockMarketViewValidator.validateModifyNonStopLimitOfferComponents(offerIdComboBox, priceTf, quantityTf);
                doModifyOffer(BigDecimal.ZERO);
            }
        } catch (StockMarketValidationException e) {
            NotificationFactory.generateNotification(e.getMessage(), Type.ERROR_MESSAGE);
        } catch (Exception e) {
            NotificationFactory.generateNotification(e.getMessage(), Type.ERROR_MESSAGE);
        }
    }

    private void doModifyOffer(BigDecimal stopLimitValue) {
        if (WindowType.isBuyOffer(windowType)) {
            doModifyBuyOffer(stopLimitValue);
        } else {
            doModifySellOffer(stopLimitValue);
        }
        closeModifyOfferWindow();
    }

    private void doModifySellOffer(BigDecimal stopLimitValue) {
        doModifySellOfferPriceAndQuantity();
        presenter.doModifySellOfferActivationPrice((Long) offerIdComboBox.getConvertedValue(), stopLimitValue);
        view.doListSellOffers();
    }

    private void doModifyBuyOffer(BigDecimal stopLimitValue) {
        doModifyBuyOfferPriceAndQuantity();
        presenter.doModifyBuyOfferActivationPrice((Long) offerIdComboBox.getConvertedValue(), stopLimitValue);
        view.doListBuyOffers();
    }

    private void doModifyBuyOfferPriceAndQuantity() {
        presenter.doModifyBuyOfferPrice((Long) offerIdComboBox.getConvertedValue(), (BigDecimal) priceTf.getConvertedValue());
        presenter.doModifyBuyOfferQuantity((Long) offerIdComboBox.getConvertedValue(), (Integer) quantityTf.getConvertedValue());
    }

    private void doModifySellOfferPriceAndQuantity() {
        presenter.doModifySellOfferPrice((Long) offerIdComboBox.getConvertedValue(), (BigDecimal) priceTf.getConvertedValue());
        presenter.doModifySellOfferQuantity((Long) offerIdComboBox.getConvertedValue(), (Integer) quantityTf.getConvertedValue());
    }

    private void closeModifyOfferWindow() {
        view.initUserOfferComponents(userDataLayout, windowType);
        this.close();
    }

    private void deleteOffer() {
        try {
            StockMarketViewValidator.validateDeleteOfferComponents(offerIdComboBox);
            if (WindowType.isBuyOffer(windowType)) {
                doDeleteBuyOffer();
            } else {
                doDeleteSellOffer();
            }
            this.close();
        } catch (StockMarketValidationException e) {
            NotificationFactory.generateNotification(e.getMessage(), Type.ERROR_MESSAGE);
        } catch (Exception e) {
            NotificationFactory.generateNotification(e.getMessage(), Type.ERROR_MESSAGE);
        }
    }

    private void doDeleteSellOffer() {
        presenter.doDeleteSellOffer((Long) offerIdComboBox.getValue());
        view.initUserOfferComponents(userDataLayout, windowType);
        view.doListSellOffers();
    }

    private void doDeleteBuyOffer() {
        presenter.doDeleteBuyOffer((Long) offerIdComboBox.getValue());
        view.initUserOfferComponents(userDataLayout, windowType);
        view.doListBuyOffers();
    }

}
