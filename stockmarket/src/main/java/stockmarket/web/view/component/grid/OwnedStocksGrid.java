package stockmarket.web.view.component.grid;

import java.math.BigDecimal;
import java.util.Map;

import com.vaadin.ui.Grid;

import stockmarket.domain.entities.Stock;
import stockmarket.web.view.BigDecimalToStringConverter;

public class OwnedStocksGrid extends Grid {
    /**
     * 
     */
    private static final long serialVersionUID = 4969154033442820729L;

    public OwnedStocksGrid(Map<Stock, Integer> ownedStocks) {
        super();
        this.setReadOnly(true);
        this.setSelectionMode(SelectionMode.NONE);
        addColumns();
        addStockValues(ownedStocks);
        this.setSizeFull();
    }

    private void addColumns() {
        this.addColumn("Részvény neve", Object.class);
        this.addColumn("Részvényt kibocsátó neve", Object.class);
        this.addColumn("Aktuális ár", Object.class);
        this.addColumn("Birtokolt mennyiség", Object.class);
        this.addColumn("Összesített érték", Object.class);
    }

    private void addStockValues(Map<Stock, Integer> ownedStocks) {
        for (Map.Entry<Stock, Integer> mapit : ownedStocks.entrySet()) {
            BigDecimal currStockValue = mapit.getKey().getCurrvalue();
            Integer stockQuantity = mapit.getValue();
            this.addRow(mapit.getKey().getName(), mapit.getKey().getCname(), BigDecimalToStringConverter.convertToString(currStockValue), stockQuantity,
                    BigDecimalToStringConverter.convertToString(currStockValue.multiply(new BigDecimal(stockQuantity))));
        }
    }

}
