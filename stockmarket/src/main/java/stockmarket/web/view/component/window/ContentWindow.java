package stockmarket.web.view.component.window;

import com.vaadin.ui.Window;

public class ContentWindow extends Window {
    
    /**
     * 
     */
    private static final long serialVersionUID = -2488068800114974736L;

    public ContentWindow(){
        super();
        setDefaultProperties();
    }

    private void setDefaultProperties() {
        this.center();
        this.setClosable(true);
        this.setResizable(true);
    }

    public ContentWindow(String caption, String width, String height, boolean isModal) {
        super(caption);
        this.setWidth(width);
        this.setHeight(height);
        this.setModal(isModal);
        setDefaultProperties();
    }

}
