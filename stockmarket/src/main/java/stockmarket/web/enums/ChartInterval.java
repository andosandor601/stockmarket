package stockmarket.web.enums;

public enum ChartInterval {
    DAY, WEEK, MONTH
}
