package stockmarket.web.enums;

public enum WindowType {
    SELL("Eladási"), BUY("Vételi");

    private final String windowCaption;

    private WindowType(String windowCaption) {
        this.windowCaption = windowCaption;
    }

    public String getWindowCaption() {
        return windowCaption;
    }
    
    public static boolean isBuyOffer(WindowType windowType) {
        return windowType.equals(WindowType.BUY);
    }

}
