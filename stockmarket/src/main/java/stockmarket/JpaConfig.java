package stockmarket;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class JpaConfig {

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        // local
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/stockmarket" + "?useSSL=false" + "&allowPublicKeyRetrieval=true" + "&useUnicode=true"
                + "&useJDBCCompliantTimezoneShift=true" + "&useLegacyDatetimeCode=false" + "&serverTimezone=UTC");
        dataSource.setUsername("root");
        dataSource.setPassword("root");

        // heroku - postgres

        /*
         * dataSource.setDriverClassName("org.postgresql.Driver"); dataSource.setUrl(
         * "jdbc:postgresql://ec2-176-34-111-152.eu-west-1.compute.amazonaws.com:5432/d1htfuh9p3stce?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory"
         * ); dataSource.setUsername("dkkuvpqpezcouy"); dataSource.setPassword("a23327cb81629c4fa1d0e429cf39561d171d5f2c31003d84e6c8a3493b09b3d1");
         */

        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("stockmarket.domain.entities");
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());
        return em;
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        properties.setProperty("hibernate.show_sql", "true");
        return properties;
    }

    /*
     * for postgres (heroku) Properties additionalProperties() { Properties properties = new Properties(); properties.setProperty("hibernate.hbm2ddl.auto",
     * "create-drop"); properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect"); properties.setProperty("hibernate.show_sql",
     * "true"); return properties; }
     */

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

}
