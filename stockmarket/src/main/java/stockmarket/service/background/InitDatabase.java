package stockmarket.service.background;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import stockmarket.domain.entities.Stock;
import stockmarket.service.exceptions.UserServiceException;
import stockmarket.service.interfaces.CoreService;
import stockmarket.service.interfaces.UserService;

public class InitDatabase {

    @Autowired
    private UserService userService;

    @Autowired
    private CoreService coreService;

    private List<Stock> doListAllStock() throws UserServiceException {
        return userService.getAllStock();
    }

    @Transactional
    public void updateStocks() {
        List<Stock> stocks = doListAllStock();
        for (Stock stock : stocks) {
            String name = stock.getName().toUpperCase();
            List<String> data = new HttpClientSnippet().downloadData(name);
            data.stream().skip(Math.max(0, data.size() - 300)).forEach((e) -> coreService.addNewChart(e, stock));
        }
        updateStockPrice();
    }

    @Transactional
    public void initStocks() {
        coreService.addNewStockMarket("BÉT", 6, 20);
        coreService.addNewStock("BÉT", "OTP", "OTP", 10000);
        coreService.addNewStock("BÉT", "MOL", "MOL", 10000);
        coreService.addNewStock("BÉT", "RICHTER", "RICHTER GEDEON", 5000);
        coreService.addNewStock("BÉT", "MTELEKOM", "MAGYAR TELEKOM", 1000);
        updateStocks();

    }

    private void updateStockPrice() {
        coreService.modifyStockCurrValue();
    }
}
