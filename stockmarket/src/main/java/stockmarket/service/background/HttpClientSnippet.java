package stockmarket.service.background;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;

public class HttpClientSnippet {
    private final String url = "http://www.portfolio.hu/reszveny/adatletoltes.php?typ=txt&rv=";

    public HttpClientSnippet() {
        super();
    }

    public List<String> downloadData(String stockname) {

        try (CloseableHttpClient httpClient = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build()) {
            URI uri = new URI(url.concat(stockname));
            HttpGet httpGet = new HttpGet(uri);

            List<String> downloaded = httpClient.execute(httpGet, new DownloadResponeHandler());
            return downloaded;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    static class DownloadResponeHandler implements ResponseHandler<List<String>> {

        public DownloadResponeHandler() {
            super();
        }

        @Override
        public List<String> handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            List<String> content = new ArrayList<>();
            String line;
            while ((line = reader.readLine()) != null)
                content.add(line);
            reader.close();
            return content;
        }
    }

}
