package stockmarket.service.exceptions;

public class CoreServiceException extends RuntimeException {

    public CoreServiceException() {
        super();
    }

    public CoreServiceException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }

    public CoreServiceException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public CoreServiceException(String arg0) {
        super(arg0);
    }

    public CoreServiceException(Throwable arg0) {
        super(arg0);
    }
}
