package stockmarket.service.validator;

import stockmarket.domain.entities.BuyOffer;
import stockmarket.domain.entities.SellOffer;
import stockmarket.domain.entities.StockMarket;
import stockmarket.service.exceptions.CoreServiceException;

public class CoreServiceValidator {

    public static void validateBuyOffer(BuyOffer offer) {
        if (offer == null) {
            throw new CoreServiceException("Null offereket nem lehet befejezni");
        }
    }

    public static void sellOfferVerification(SellOffer soffer) throws CoreServiceException {
        if (soffer == null || soffer.getStock() == null) {
            throw new CoreServiceException("Null offereket nem lehet befejezni");
        }
        if (soffer.getUser().getOwnedstocks() == null) {
            throw new CoreServiceException("A felhasználónak nincsenek saját részvényei");
        }
        if (!soffer.getUser().getOwnedstocks().containsKey(soffer.getStock())) {
            throw new CoreServiceException("A felhasználónak nincs ilyen birtokolt részvénye ergo nem tudja eladni");
        }
        if (soffer.getQuantity() > soffer.getUser().getOwnedstocks().get(soffer.getStock())) {
            throw new CoreServiceException("A felhasználó nem birtokol ennyi részvényt");
        }
    }

    public static void validateStockMarket(StockMarket stockmarket) {
        if (stockmarket == null) {
            throw new CoreServiceException("Nulla a stockmarket");
        }
    }
    
    

}
