package stockmarket.service.validator;

import java.math.BigDecimal;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;

import stockmarket.domain.entities.BuyOffer;
import stockmarket.domain.entities.Chart;
import stockmarket.domain.entities.SellOffer;
import stockmarket.domain.entities.Stock;
import stockmarket.domain.entities.User;
import stockmarket.service.exceptions.UserServiceException;
import stockmarket.service.impl.util.CreateOfferRequest;

public class UserServiceValidator {

    public static void validateSignupFields(String userName, String password, String email, String bankAccount) {
        if (userName == null || email == null || password == null || bankAccount == null) {
            throw new UserServiceException("Üres input mezők");
        }
    }

    public static void validateCreatedUser(User user) {
        if (user == null) {
            throw new UserServiceException("Ilyen nevű felhasználó már létezik");
        }
    }

    public static void validateLoginFields(String userName, String password) {
        if (userName == null || password == null) {
            throw new UserServiceException("nullértékes bejelentkezés");
        }
    }

    public static void authenticateUser(User user, String password) {
        if (user == null || !BCrypt.checkpw(password, user.getPassword())) {
            throw new UserServiceException("Hibás felhasználóinév vagy jelszó");
        }
    }

    public static void validateDepositMoney(BigDecimal value) {
        if (value.signum() < 0) {
            throw new UserServiceException("Negatív összeget nem lehet feltölteni a számlára");
        }
    }

    public static void validateWithdrawMoney(BigDecimal value, BigDecimal newMoney, User user) {
        if (value.signum() < 0) {
            throw new UserServiceException("Negatív összeget nem lehet leemelni a számláról");
        }
        if (newMoney.signum() < 0) {
            throw new UserServiceException("Nem lehet nagyobb összeget leemelni a számláról, mint amennyi rajta van");
        }
        if (!user.getBuyoffers().isEmpty() && newMoney.subtract(user.getAllbuyoffersvalue()).signum() < 0) {
            throw new UserServiceException("Nem lehet ekkora összeget leemelni a számláról, mert nem marad fedezet a részvényvásárlásokra");
        }
    }

    public static void validateChartLoad(List<Chart> chart) {
        if (chart == null) {
            throw new UserServiceException("Nincs ilyen grafikon");
        }
    }

    public static void verifyBuyOffer(BuyOffer buyoffer) throws UserServiceException {
        if (buyoffer.getUser().getAllbuyoffersvalue().compareTo(buyoffer.getUser().getMoney()) >= 0) {
            throw new UserServiceException("Nincs fedezet a számlán ennyi részvény megvételére");
        }
        if (buyoffer.getPrice().multiply(new BigDecimal(buyoffer.getQuantity()))
                .compareTo(buyoffer.getUser().getMoney().subtract(buyoffer.getUser().getAllbuyoffersvalue())) > 0) {
            throw new UserServiceException("Nincs fedezet a számlán ennyi részvény megvételére");
        }
        if (buyoffer.getQuantity() <= 0) {
            throw new UserServiceException("Csak pozitív számú részvényt lehet vásárolni");
        }
        if (buyoffer.getPrice().signum() <= 0) {
            throw new UserServiceException("Valószínüleg nem lesz negatív ára a részvénynek");
        }
    }

    public static void verifySellOffer(SellOffer selloffer) throws UserServiceException {
        if (selloffer.getQuantity() <= 0) {
            throw new UserServiceException("Csak pozitív számú részvényt lehet eladni");
        }
        if (selloffer.getPrice().signum() <= 0) {
            throw new UserServiceException("Valószínűleg nem lesz negatív ára a részvénynek");
        }
        if (selloffer.getStock() == null) {
            throw new UserServiceException("Nincs ilyen ajánlat");
        }
        if (selloffer.getUser().getOwnedstocks() == null) {
            throw new UserServiceException("Nem birtokol részvényt a felhasználó");
        }
        if (!selloffer.getUser().getOwnedstocks().containsKey(selloffer.getStock())) {
            throw new UserServiceException("Nem birtokol ilyen rézvényt a felhasználó");
        } else if (selloffer.getUser().getOwnedstocks().get(selloffer.getStock()) < selloffer.getQuantity()) {
            throw new UserServiceException("Nem birtokol ennyi részvényt a felhasználó");
        }
        int sum = 0;
        for (SellOffer sofferIt : selloffer.getUser().getSelloffers()) {
            if (sofferIt.getStock().equals(selloffer.getStock())) {
                sum += sofferIt.getQuantity();
            }
        }
        if (selloffer.getUser().getOwnedstocks().get(selloffer.getStock()) - sum < selloffer.getQuantity()) {
            throw new UserServiceException("Az új eladási ajánlat száma meghaladja a még eladható részvények számát");
        }
    }

    public static void validateBuyOfferRequest(CreateOfferRequest buyofferRequest) {
        if ((buyofferRequest.getPrice().signum() <= 0) || buyofferRequest.getActivationprice().signum() <= 0) {
            throw new UserServiceException("Valószínűleg nem lesz negatív ára a részvénynek");
        }
        if (buyofferRequest.getPrice().multiply(new BigDecimal(buyofferRequest.getQuantity())).compareTo(buyofferRequest.getUser().getMoney()) > 0) {
            throw new UserServiceException("Nincs fedezet a számlán ennyi részvény megvételére");
        }
    }

    public static void validateSellOfferRequest(CreateOfferRequest sellofferRequest) {
        if ((sellofferRequest.getPrice().signum() <= 0) || sellofferRequest.getActivationprice().signum() <= 0) {
            throw new UserServiceException("Valószínűleg nem lesz negatív ára a részvénynek");
        }
    }

    public static void verifyBuyOfferDelete(BuyOffer buyoffer) {
        if (buyoffer == null || buyoffer.getUser() == null) {
            throw new UserServiceException("Nincs ilyen ajánlat");
        }
        if (!buyoffer.getUser().getBuyoffers().contains(buyoffer)) {
            throw new UserServiceException("A felhasználónak, nincs ilyen ajánlata");
        }
    }

    public static void verifySellOfferDelete(SellOffer selloffer) {
        if (selloffer == null || selloffer.getUser() == null) {
            throw new UserServiceException("Nincs ilyen ajánlat");
        }
        if (!selloffer.getUser().getSelloffers().contains(selloffer)) {
            throw new UserServiceException("A felhasználónak, nincs ilyen ajánlata");
        }
    }

    public static void validateModifiedNewPriceOfOffer(BigDecimal newPrice) {
        if (newPrice.signum() <= 0) {
            throw new UserServiceException("Nem lehet negatív árat megadni");
        }
    }

    public static void validateModifyBuyOfferQuantity(BuyOffer buyoffer, int newQuantity) {
        if (newQuantity <= 0) {
            throw new UserServiceException("Nem lehet negatív mennyiséget megadni");
        }
        if (buyoffer == null) {
            throw new UserServiceException("Nincs ilyen ajánlat");
        }
    }

    public static void validateModifySellOfferRequest(SellOffer selloffer, int newQuantity) {
        if (newQuantity <= 0) {
            throw new UserServiceException("Nem lehet negatív árat megadni");
        }
        if (selloffer == null) {
            throw new UserServiceException("Nincs ilyen ajánlat");
        }
    }

    public static void validateModifiedNewActivationPriceOfOffer(BigDecimal newActivationPrice) {
        if (newActivationPrice.signum() < 0) {
            throw new UserServiceException("Nem lehet negatív árat megadni");
        }
    }

    public static void validateStock(Stock stock) {
        if (stock == null) {
            throw new UserServiceException("Nincs ilyen részvény");
        }
    }

    public static void validateUser(User user) {
        if (user == null) {
            throw new UserServiceException("Nincs ilyen felhasználó");
        }
    }

    public static void validateBuyOffer(BuyOffer buyoffer) {
        if (buyoffer == null) {
            throw new UserServiceException("Nincs ilyen buyoffer");
        }
    }

    public static void validateSellOffer(SellOffer selloffer) {
        if (selloffer == null) {
            throw new UserServiceException("Nincs ilyen selloffer");
        }
    }

}
