package stockmarket.service.interfaces;

import java.math.BigDecimal;
import java.util.List;

import stockmarket.domain.entities.*;
import stockmarket.service.exceptions.UserServiceException;
import stockmarket.service.impl.util.CreateOfferRequest;

/**
 * A felhasználói interakciókat leíró Interface
 * 
 * @author Andó Sándor Zsolt
 *
 */
public interface UserService {

    /**
     * A felhasználó regisztrációja
     * 
     * @param userName:
     *            leendő felhnev
     * @param password:leendő
     *            jelszó
     * @param email:
     *            leendő e-mail
     * @param bankAccount:
     *            leendő számlaszám
     * @return User: a regisztrált felhasználó
     * @throws UserServiceException:
     *             hiba ha már létezik ilyen felhnevvel rendelkező user
     */
    User signup(String userName, String password, String email, String bankAccount) throws UserServiceException;

    /**
     * A felhasználó bejelentkezése
     * 
     * @param userName:
     *            a felhasználó neve
     * @param password:
     *            a felhasználó által megadott jelszó
     * @return : User a felhasználóval térnénk vissza, amennyiben a felhnév jelszó páros létezik,
     * @throws UserServiceException:
     *             valamilyen hiba esetén kivételt dobunk(nem megfelelő adatok, hiányzó adatok)
     */
    User login(String userName, String password) throws UserServiceException;

    /**
     * 
     * A felhasználó pénzt tölt fel a számlájára
     * 
     * @param user:
     *            Melyik felhasználó tesz fel pénzt
     * @param value:
     *            Mennyi pénzt tesz fel
     * @return :float Az összes számlán lévő pénz
     * @throws UserServiceException:
     *             valamilyen hiba esetén kivételt dobunk(hiba ttörténik az utalásnál, hiányzó érték)
     */
    User depositMoney(User user, BigDecimal value) throws UserServiceException;

    /**
     * A felhasználó pénzt vesz le a számlájáról
     * 
     * @param user:
     *            Melyik felhasználó teszi mindezt
     * @param value:
     *            Mennyit akar levenni
     * @return: float az összes fent lévő pénzmennyiség
     * @throws valamilyen
     *             hiba esetén kivételt dobunk(hiba történik az ututalásnál, hiányzó érték)
     */
    User withdrawMoney(User user, BigDecimal value) throws UserServiceException;

    /**
     * A felhasználó grafikonokat akar megtekinteni
     * 
     * @param stockname:
     *            Milyen részvényre kíváncsi
     * @return: Chart a diagram
     * @throws UserServiceException:
     *             valamilyen hiba esetén kivételt dobunk(nem létező részvény)
     */
    List<Chart> loadChart(Stock stock) throws UserServiceException;

    /**
     * A felhasználó részvényvásárlási szándéka
     * 
     * @param buyofferRequest:
     *            A részvényvásárláshoz szükséges információk
     * @return: Buyoffer: Részvényvásárlási szándék
     * @throws UserServiceException:
     *             valamilyen hiba esetén kivételt dobunk(hiányzó adat)
     */
    BuyOffer newBuyOffer(CreateOfferRequest buyofferRequest) throws UserServiceException;

    /**
     * A felhasználó részvényeladási szándéka
     * 
     * @param sellofferRequest:
     *            A részvényeladáshoz szükséges információk
     * @return: Selloffer: Részvényeladási szándék
     * @throws UserServiceException:
     *             valamilyen hiba esetén kivételt dobunk(hiányzó adat)
     */
    SellOffer newSellOffer(CreateOfferRequest sellofferRequest) throws UserServiceException;

    /**
     * A felhasználó részvényvásárlási szándéka stoppal
     * 
     * @param buyofferRequest:
     *            A részvényvásárláshoz szükséges információk
     * @return: Buyoffer: Részvényvásárlási szándék
     * @throws UserServiceException:
     *             valamilyen hiba esetén kivételt dobunk(hiányzó adat)
     */
    BuyOffer newStopBuyOffer(CreateOfferRequest buyofferRequest) throws UserServiceException;

    /**
     * A felhasználó részvényeladási szándéka stoppal
     * 
     * @param sellofferRequest:
     *            A részvényeladáshoz szükséges információk
     * @return: Selloffer: Részvényeladási szándék
     * @throws UserServiceException:
     *             valamilyen hiba esetén kivételt dobunk(hiányzó adat)
     */
    SellOffer newStopSellOffer(CreateOfferRequest sellofferRequest) throws UserServiceException;

    /**
     * A felhasználó visszavonja a vvásárlási szándékát
     * 
     * @param buyofferId:
     *            a visszavonni kívánt vásárlás azonosítója
     * @throws UserServiceException:
     *             a hiba
     */
    void deleteBuyOffer(long buyofferId) throws UserServiceException;

    /**
     * A felhasználó egy eladási szándékot visszavon
     * 
     * @param sellofferId:Az
     *            az üzlet azonosítója, amit a felhasználó visszaakar vonni
     * @throws UserServiceException:
     *             a hiba
     */
    void deleteSellOffer(long sellofferId) throws UserServiceException;

    /**
     * A felhasználó észbekap, hogy mégsem olyan áron akarja az üzletet lebonyolítani, mint azt eredetileg akarta
     * 
     * @param buyofferId:
     *            Módosítandó BuyOffer azonosító
     * @param newPrice:
     *            új ár meghatározása
     * @return BuyOffer: új/Módosított BuyOfferrel térünk vissza
     */
    BuyOffer modifyBuyOfferPrice(long buyofferId, BigDecimal newPrice);

    /**
     * A felhasználó észbekap, hogy mégsem olyan áron akarja az üzletet lebonyolítani, mint azt eredetileg akarta
     * 
     * @param sellofferId:
     *            Módosítandó SellOffer azonosító
     * @param newPrice:
     *            új ár meghatározása
     * @return: új/Módosított BuyOfferrel térünk vissza
     */
    SellOffer modifySellOfferPrice(long sellofferId, BigDecimal newPrice);

    /**
     * A felhasználó módosítani akarja a vásárolni kívánt részvény mennyiségét
     * 
     * @param buyofferId:
     *            Módosítandó offer azonosító
     * @param newQuantity:
     *            új mennyiség
     * @return BuyOffer: módosított offer
     */
    BuyOffer modifyBuyOfferQuantity(long buyofferId, int newQuantity);

    /**
     * A felhasználó módosítani akarja a eladni kívánt részvény mennyiségét
     * 
     * @param sellofferId:
     *            Módosítandó offer azonosító
     * @param newQuantity:
     *            új mennyiség
     * @return SellOffer: módosított offer
     */
    SellOffer modifySellOfferQuantity(long sellofferId, int newQuantity);

    /**
     * A felhasználónak visszaadjuk az összes elérhtő részvényt
     * 
     * @return: List<Stock> a részvények listája
     */
    List<Stock> getAllStock();

    /**
     * Egy részvényt akarunk keresni név és cégnév alapján
     * 
     * @param stockName:
     *            részvény neve
     * @param companyName:
     *            kibocsátó neve
     * @return Stock: a megtalált részvény
     */
    Stock findStock(String stockName, String companyName) throws UserServiceException;

    /**
     * Egy user visszaadása username alapján
     * 
     * @param userName:
     *            a felhasználó neve
     * @return User: a megtalált user
     */
    User findUserByName(String userName) throws UserServiceException;

    /**
     * Egy buyoffer megkeresése az azonosíja alapján
     * 
     * @param buyofferId:
     *            buyoffer azonosító
     * @return BuyOffer: a megtalált offer
     * @throws UserServiceException
     */
    BuyOffer findBuyOfferById(long buyofferId) throws UserServiceException;

    /**
     * Egy selloffer megkeresése az azonosíja alapján
     * 
     * @param sellofferId:
     *            selloffer azonosító
     * @return SellOffer: a megtalált offer
     * @throws UserServiceException
     */
    SellOffer findSellOfferById(long sellofferId) throws UserServiceException;

    /**
     * Egy Stoplimites BuyOffernek az aktivációsár módosítása
     * 
     * @param buyofferId:
     *            BuyOffer azonosítója
     * @param newActivationPrice:
     *            új aktivációsár
     * @return BuyOffer: Módosított BuyOffer
     */
    BuyOffer modifyBuyOfferActivationPrice(long buyofferId, BigDecimal newActivationPrice) throws UserServiceException;

    /**
     * Egy Stoplimites SellOffernek az aktivációsár módosítása
     * 
     * @param sellofferId:
     *            SellOffer azonosítója
     * @param newActivationPrice:
     *            új aktivációsár
     * @return SellOffer: Módosított SellOffer
     * @throws UserServiceException
     */
    SellOffer modifySellOfferActivationPrice(long sellofferId, BigDecimal newActivationPrice) throws UserServiceException;

}
