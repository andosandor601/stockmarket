package stockmarket.service.interfaces;

import java.util.List;

import stockmarket.domain.entities.Stock;
import stockmarket.service.exceptions.CoreServiceException;

/**
 * Minden más a program által lefutó metódusok, amelyek nem kötődnek szervesen a userinterakciókhoz
 * 
 * @author Andó Sándor Zsolt
 *
 */
public interface CoreService {
    /**
     * üzlet keresése, van-e eladási és vásárlási szándék ugyanarra a részvényre *
     * 
     * @throws CoreServiceException:
     *             Ha bármilyen hiba törtönik a keresés közben
     */
    void findTransaction() throws CoreServiceException;

    /**
     * A részvény aktuális értéke, mindig az az érték lesz, amilyen áron a legutóbbi kereskedés zajlott, ezt itt állítjuk be
     */
    void modifyStockCurrValue();

    /**
     * A metódus megkeresi az összes részvényt a rendszerben
     * 
     * @return Set<Stock>: Az összes megtalált részvény
     */
    List<Stock> findAllStock();

    /**
     * A metódus visszatér, a grafikon legutolsó dátumával
     * 
     * @return String: A grafikon legutolsó dátuma
     */
    String getLastDate(Stock stock);

    /**
     * Új chart készítése
     * 
     * @param line:
     *            chart adatait tartalmazó string
     * @param stock:
     *            melyik részvényhez tartozik a chart
     */
    void addNewChart(String line, Stock stock);

    /**
     * Új stockmarket felvétele az adtabázisba
     * 
     * @param stockmarketName:
     *            stockmarekt neve
     * @param open:
     *            nyitó óra
     * @param close:
     *            záróra
     */
    void addNewStockMarket(String stockmarketName, int open, int close);

    /**
     * Új Stock felvétele az adatbázisba
     * 
     * @param stockmarketName:
     *            stockmarket neve
     * @param stockName:
     *            részvény neve
     * @param companyName:
     *            részvénykibocsátó neve
     * @param freeQauntity:
     *            piaci áron kibocsátótól megvásárolható mennyiség
     */
    void addNewStock(String stockmarketName, String stockName, String companyName, int freeQauntity);

}
