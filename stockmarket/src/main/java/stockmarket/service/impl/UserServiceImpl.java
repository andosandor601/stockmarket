package stockmarket.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import stockmarket.dao.interfaces.JpaUserServiceDao;
import stockmarket.domain.entities.BuyOffer;
import stockmarket.domain.entities.Chart;
import stockmarket.domain.entities.SellOffer;
import stockmarket.domain.entities.Stock;
import stockmarket.domain.entities.StockMarket;
import stockmarket.domain.entities.User;
import stockmarket.service.exceptions.UserServiceException;
import stockmarket.service.impl.util.CreateOfferRequest;
import stockmarket.service.interfaces.UserService;
import stockmarket.service.validator.UserServiceValidator;

public class UserServiceImpl implements UserService {

    private final JpaUserServiceDao jpauserservicedao;

    @Autowired
    public UserServiceImpl(JpaUserServiceDao jpadao) {
        super();
        this.jpauserservicedao = jpadao;
    }

    @Transactional
    @Override
    public User signup(String userName, String password, String email, String bankAccount) throws UserServiceException {
        UserServiceValidator.validateSignupFields(userName, password, email, bankAccount);
        User user = createUser(userName, password, email, bankAccount);
        user = jpauserservicedao.save(user);
        UserServiceValidator.validateCreatedUser(user);
        return user;
    }

    private User createUser(String userName, String password, String email, String bankAccount) {
        User user = new User();
        user.setName(userName);
        String pw = BCrypt.hashpw(password, BCrypt.gensalt());
        user.setPassword(pw);
        user.setEmail(email);
        user.setBankaccount(bankAccount);
        user.setAlloffersvalue(BigDecimal.ZERO);
        user.setBuyoffers(new HashSet<>());
        user.setSelloffers(new HashSet<>());
        user.setMoney(BigDecimal.ZERO);
        user.setOwnedstocks(new HashMap<>());
        return user;
    }

    @Transactional
    @Override
    public User login(String userName, String password) throws UserServiceException {
        UserServiceValidator.validateLoginFields(userName, password);
        User user = jpauserservicedao.findById(User.class, userName);
        UserServiceValidator.authenticateUser(user, password);
        return user;
    }

    @Transactional
    @Override
    public User depositMoney(User user, BigDecimal value) throws UserServiceException {
        BigDecimal oldMoney = user.getMoney();
        UserServiceValidator.validateDepositMoney(value);
        user.setMoney(oldMoney.add(value));
        return jpauserservicedao.update(user);
    }

    @Transactional
    @Override
    public User withdrawMoney(User user, BigDecimal value) throws UserServiceException {
        BigDecimal oldMoney = user.getMoney();
        BigDecimal newMoney = oldMoney.subtract(value);
        UserServiceValidator.validateWithdrawMoney(value, newMoney, user);
        user.setMoney(newMoney);
        return jpauserservicedao.update(user);
    }

    @Transactional
    @Override
    public List<Chart> loadChart(Stock stock) throws UserServiceException {
        List<Chart> chart = jpauserservicedao.findChartByStock(stock);
        UserServiceValidator.validateChartLoad(chart);
        return chart;
    }

    @Transactional
    @Override
    public BuyOffer newBuyOffer(CreateOfferRequest buyofferRequest) throws UserServiceException {
        BuyOffer buyoffer = saveBuyOffer(buyofferRequest);
        return buyoffer;
    }

    @Transactional
    @Override
    public SellOffer newSellOffer(CreateOfferRequest sellofferRequest) throws UserServiceException {
        SellOffer selloffer = createSellOffer(sellofferRequest);
        User user = selloffer.getUser();
        UserServiceValidator.verifySellOffer(selloffer);
        selloffer = jpauserservicedao.save(selloffer);
        setUserStocks(selloffer, user);
        jpauserservicedao.update(user);
        return selloffer;
    }

    @Transactional
    @Override
    public BuyOffer newStopBuyOffer(CreateOfferRequest buyofferRequest) throws UserServiceException {
        UserServiceValidator.validateBuyOfferRequest(buyofferRequest);
        BuyOffer buyoffer = saveBuyOffer(buyofferRequest);
        return buyoffer;
    }

    private BuyOffer saveBuyOffer(CreateOfferRequest buyofferRequest) {
        BuyOffer buyoffer = createBuyOffer(buyofferRequest);
        UserServiceValidator.verifyBuyOffer(buyoffer);
        buyoffer.getUser()
                .setAlloffersvalue(buyoffer.getUser().getAllbuyoffersvalue().add(buyoffer.getPrice().multiply(new BigDecimal(buyoffer.getQuantity()))));
        buyoffer = jpauserservicedao.save(buyoffer);
        jpauserservicedao.update(buyofferRequest.getUser());
        return buyoffer;
    }

    @Transactional
    @Override
    public SellOffer newStopSellOffer(CreateOfferRequest sellofferRequest) throws UserServiceException {
        UserServiceValidator.validateSellOfferRequest(sellofferRequest);
        SellOffer selloffer = saveSellOffer(sellofferRequest);
        return selloffer;
    }

    private SellOffer saveSellOffer(CreateOfferRequest sellofferRequest) {
        SellOffer selloffer = createSellOffer(sellofferRequest);
        User user = selloffer.getUser();
        UserServiceValidator.verifySellOffer(selloffer);
        setUserStocks(selloffer, user);
        jpauserservicedao.update(user);
        selloffer = jpauserservicedao.save(selloffer);
        return selloffer;
    }

    private void setUserStocks(SellOffer selloffer, User user) {
        Integer newQuantity = user.getOwnedstocks().get(selloffer.getStock()) - selloffer.getQuantity();
        if (newQuantity.equals(0)) {
            user.getOwnedstocks().remove(selloffer.getStock());
        } else {
            user.getOwnedstocks().put(selloffer.getStock(), newQuantity);
        }
    }

    private BuyOffer createBuyOffer(CreateOfferRequest buyofferRequest) {
        BuyOffer buyoffer = new BuyOffer();
        buyoffer.setActivationprice(buyofferRequest.getActivationprice());
        buyoffer.setPrice(buyofferRequest.getPrice());
        buyoffer.setQuantity(buyofferRequest.getQuantity());
        buyoffer.setStock(buyofferRequest.getStock());
        buyoffer.setStockmarket(jpauserservicedao.findStockMarketByStock(buyofferRequest.getStock()));
        buyoffer.setUser(buyofferRequest.getUser());
        buyoffer.setActive(isActiveBuyOffer(buyofferRequest));
        return buyoffer;
    }

    private boolean isActiveBuyOffer(CreateOfferRequest buyofferRequest) {
        return buyofferRequest.getActivationprice().signum() == 0
                || buyofferRequest.getStock().getCurrvalue().compareTo(buyofferRequest.getActivationprice()) >= 0;
    }

    private SellOffer createSellOffer(CreateOfferRequest sellofferRequest) {
        SellOffer selloffer = new SellOffer();
        selloffer.setActivationprice(sellofferRequest.getActivationprice());
        selloffer.setPrice(sellofferRequest.getPrice());
        selloffer.setQuantity(sellofferRequest.getQuantity());
        selloffer.setStock(sellofferRequest.getStock());
        selloffer.setStockmarket(jpauserservicedao.findStockMarketByStock(sellofferRequest.getStock()));
        selloffer.setUser(sellofferRequest.getUser());
        selloffer.setActive(isActiveSellOffer(sellofferRequest));
        return selloffer;
    }

    private boolean isActiveSellOffer(CreateOfferRequest sellofferRequest) {
        return sellofferRequest.getActivationprice().signum() == 0
                || sellofferRequest.getStock().getCurrvalue().compareTo(sellofferRequest.getActivationprice()) <= 0;
    }

    @Transactional
    @Override
    public void deleteBuyOffer(long buyofferId) throws UserServiceException {
        BuyOffer buyoffer = findBuyOfferById(buyofferId);
        UserServiceValidator.verifyBuyOfferDelete(buyoffer);
        User user = buyoffer.getUser();
        user.getBuyoffers().remove(buyoffer);
        user.setAlloffersvalue(user.getAllbuyoffersvalue().subtract(buyoffer.getPrice().multiply(new BigDecimal(buyoffer.getQuantity()))));
        jpauserservicedao.delete(buyoffer);
        jpauserservicedao.update(user);
    }

    @Transactional
    @Override
    public void deleteSellOffer(long sellofferId) throws UserServiceException {
        SellOffer selloffer = findSellOfferById(sellofferId);
        UserServiceValidator.verifySellOfferDelete(selloffer);
        User user = selloffer.getUser();
        user.getSelloffers().remove(selloffer);
        Integer newQuantity = calculateNewQuantity(selloffer, user);
        user.getOwnedstocks().put(selloffer.getStock(), newQuantity);
        jpauserservicedao.update(user);
    }

    private Integer calculateNewQuantity(SellOffer selloffer, User user) {
        Integer newQuantity;
        if (user.getOwnedstocks() != null && user.getOwnedstocks().containsKey(selloffer.getStock())) {
            newQuantity = user.getOwnedstocks().get(selloffer.getStock()) + selloffer.getQuantity();
        } else {
            newQuantity = selloffer.getQuantity();
        }
        return newQuantity;
    }

    @Transactional
    @Override
    public BuyOffer modifyBuyOfferPrice(long buyofferId, BigDecimal newPrice) throws UserServiceException {
        BuyOffer buyoffer = findBuyOfferById(buyofferId);
        UserServiceValidator.validateModifiedNewPriceOfOffer(newPrice);
        buyoffer.getUser().getAllbuyoffersvalue().add(buyoffer.getPrice().multiply(new BigDecimal(buyoffer.getQuantity())));
        buyoffer.setPrice(newPrice);
        UserServiceValidator.verifyBuyOffer(buyoffer);
        buyoffer.getUser().getAllbuyoffersvalue().subtract(newPrice.multiply(new BigDecimal(buyoffer.getQuantity())));
        jpauserservicedao.update(buyoffer.getUser());
        return jpauserservicedao.update(buyoffer);

    }

    @Transactional
    @Override
    public SellOffer modifySellOfferPrice(long sellofferId, BigDecimal newPrice) throws UserServiceException {
        SellOffer selloffer = findSellOfferById(sellofferId);
        UserServiceValidator.validateModifiedNewPriceOfOffer(newPrice);
        selloffer.setPrice(newPrice);
        UserServiceValidator.verifySellOffer(selloffer);
        return jpauserservicedao.update(selloffer);
    }

    @Transactional
    @Override
    public BuyOffer modifyBuyOfferQuantity(long buyofferId, int newQuantity) throws UserServiceException {
        BuyOffer buyoffer = findBuyOfferById(buyofferId);
        UserServiceValidator.validateModifyBuyOfferQuantity(buyoffer, newQuantity);
        buyoffer.getUser().getAllbuyoffersvalue().add(buyoffer.getPrice().multiply(new BigDecimal(buyoffer.getQuantity())));
        buyoffer.setQuantity(newQuantity);
        UserServiceValidator.verifyBuyOffer(buyoffer);
        buyoffer.getUser().getAllbuyoffersvalue().subtract(buyoffer.getPrice().multiply(new BigDecimal(newQuantity)));
        jpauserservicedao.update(buyoffer.getUser());
        return jpauserservicedao.update(buyoffer);
    }

    @Transactional
    @Override
    public SellOffer modifySellOfferQuantity(long sellofferId, int newQuantity) throws UserServiceException {
        SellOffer selloffer = findSellOfferById(sellofferId);
        UserServiceValidator.validateModifySellOfferRequest(selloffer, newQuantity);
        selloffer.setQuantity(newQuantity);
        UserServiceValidator.verifySellOffer(selloffer);
        return jpauserservicedao.update(selloffer);
    }

    @Override
    @Transactional
    public BuyOffer modifyBuyOfferActivationPrice(long buyofferId, BigDecimal newActivationPrice) throws UserServiceException {
        BuyOffer buyoffer = findBuyOfferById(buyofferId);
        UserServiceValidator.validateModifiedNewActivationPriceOfOffer(newActivationPrice);
        buyoffer.getUser().getAllbuyoffersvalue().add(buyoffer.getPrice().multiply(new BigDecimal(buyoffer.getQuantity())));
        buyoffer.setActivationprice(newActivationPrice);
        buyoffer.setActive(isActiveBuyOffer(buyoffer));
        UserServiceValidator.verifyBuyOffer(buyoffer);
        jpauserservicedao.update(buyoffer.getUser());
        return jpauserservicedao.update(buyoffer);
    }

    private boolean isActiveBuyOffer(BuyOffer buyoffer) {
        return buyoffer.getActivationprice().signum() == 0 || buyoffer.getStock().getCurrvalue().compareTo(buyoffer.getActivationprice()) >= 0;
    }

    @Override
    @Transactional
    public SellOffer modifySellOfferActivationPrice(long sellofferId, BigDecimal newActivationPrice) throws UserServiceException {
        SellOffer selloffer = findSellOfferById(sellofferId);
        UserServiceValidator.validateModifiedNewActivationPriceOfOffer(newActivationPrice);
        selloffer.setActivationprice(newActivationPrice);
        selloffer.setActive(isActiveSellOffer(selloffer));
        UserServiceValidator.verifySellOffer(selloffer);
        return jpauserservicedao.update(selloffer);
    }

    private boolean isActiveSellOffer(SellOffer selloffer) {
        return selloffer.getActivationprice().signum() == 0 || selloffer.getStock().getCurrvalue().compareTo(selloffer.getActivationprice()) <= 0;
    }

    @Override
    public List<Stock> getAllStock() {
        List<Stock> allStock = new ArrayList<>();
        List<StockMarket> allStockmarket = jpauserservicedao.findAll(StockMarket.class);
        for (StockMarket stockmarket : allStockmarket) {
            allStock.addAll(stockmarket.getStocks());
        }
        return allStock;
    }

    @Override
    public Stock findStock(String stockName, String companyName) throws UserServiceException {
        Stock stock = jpauserservicedao.findStockByNameAndCname(stockName, companyName);
        UserServiceValidator.validateStock(stock);
        return stock;
    }

    @Override
    public User findUserByName(String userName) throws UserServiceException {
        User user = jpauserservicedao.findById(User.class, userName);
        UserServiceValidator.validateUser(user);
        return user;
    }

    @Override
    public BuyOffer findBuyOfferById(long buyofferId) throws UserServiceException {
        BuyOffer buyoffer = jpauserservicedao.findById(BuyOffer.class, buyofferId);
        UserServiceValidator.validateBuyOffer(buyoffer);
        return buyoffer;
    }

    @Override
    public SellOffer findSellOfferById(long sellofferId) throws UserServiceException {
        SellOffer selloffer = jpauserservicedao.findById(SellOffer.class, sellofferId);
        UserServiceValidator.validateSellOffer(selloffer);
        return selloffer;
    }

}
