package stockmarket.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import stockmarket.dao.interfaces.JpaCoreServiceDao;
import stockmarket.domain.entities.BuyOffer;
import stockmarket.domain.entities.Chart;
import stockmarket.domain.entities.Log;
import stockmarket.domain.entities.SellOffer;
import stockmarket.domain.entities.Stock;
import stockmarket.domain.entities.StockMarket;
import stockmarket.domain.entities.User;
import stockmarket.service.exceptions.CoreServiceException;
import stockmarket.service.interfaces.CoreService;
import stockmarket.service.validator.CoreServiceValidator;

public class CoreServiceImpl implements CoreService {

    private final JpaCoreServiceDao jpacoreservicedao;

    @Autowired
    public CoreServiceImpl(JpaCoreServiceDao jpadao) {
        super();
        this.jpacoreservicedao = jpadao;
    }

    @Override
    @Transactional(noRollbackFor = Exception.class)
    public void findTransaction() throws CoreServiceException {
        jpacoreservicedao.findAll(StockMarket.class).forEach((stockmarket) -> {
            findMatchingOffersByStockMarket(stockmarket);
        });
    }

    private void findMatchingOffersByStockMarket(StockMarket stockmarket) {
        Set<BuyOffer> buyoffers = stockmarket.getBuyers();
        Set<SellOffer> selloffers = stockmarket.getSellers();
        if (buyoffers != null) {
            findMatchingOffers(buyoffers, selloffers);
        }
    }

    private void findMatchingOffers(Set<BuyOffer> buyoffers, Set<SellOffer> selloffers) {
        Calendar currTime = Calendar.getInstance();
        Iterator<BuyOffer> buyofferit = buyoffers.iterator();
        findMatchesBetweenOffersNaiveAlgorithm(selloffers, currTime, buyofferit);
    }

    private void findMatchesBetweenOffersNaiveAlgorithm(Set<SellOffer> selloffers, Calendar currTime, Iterator<BuyOffer> buyofferit) {
        while (buyofferit.hasNext()) {
            BuyOffer currBuyOffer = (BuyOffer) buyofferit.next();
            if (!currBuyOffer.isActive()) {
                continue;
            }
            if (isExistingSellOffer(selloffers)) {
                Iterator<SellOffer> sellofferit = selloffers.iterator();
                while (sellofferit.hasNext()) {
                    SellOffer currSellOffer = (SellOffer) sellofferit.next();
                    if (notMatchingSellOffer(currBuyOffer, currSellOffer)) {
                        continue;
                    } else if (hasBuyOfferHigherPriceThanSellOffer(currBuyOffer, currSellOffer)) {
                        break;
                    }
                    deal(currBuyOffer, currSellOffer, currBuyOffer.getStock(), currTime);
                }
            }
            if (isExistingOrphanStockWhichMatches(currBuyOffer)) {
                deal(currBuyOffer, currBuyOffer.getStock(), currTime);
            }
        }
    }

    private boolean isExistingOrphanStockWhichMatches(BuyOffer currBuyOffer) {
        return currBuyOffer.getPrice().compareTo(currBuyOffer.getStock().getCurrvalue()) >= 0;
    }

    private boolean hasBuyOfferHigherPriceThanSellOffer(BuyOffer currBuyOffer, SellOffer currSellOffer) {
        return currBuyOffer.getPrice().compareTo(currSellOffer.getPrice()) < 0;
    }

    private boolean notMatchingSellOffer(BuyOffer currBuyOffer, SellOffer currSellOffer) {
        return !(currSellOffer.isActive() && currBuyOffer.getStock().equals(currSellOffer.getStock()));
    }

    private boolean isExistingSellOffer(Set<SellOffer> selloffers) {
        return selloffers != null && !selloffers.isEmpty();
    }

    private void deal(BuyOffer buyoffer, Stock stock, Calendar actTime) {
        if (hasBuyOfferLessOrEqualQuantityThanOrphanStockQuantity(buyoffer)) {
            stock.setFree_quantity(stock.getFree_quantity() - buyoffer.getQuantity());
            jpacoreservicedao.update(stock);
            completeBuy(buyoffer, buyoffer.getStock().getCurrvalue());
        }
    }

    private void deal(BuyOffer buyoffer, SellOffer selloffer, Stock stock, Calendar actTime) {
        if (haveEqualQauntity(buyoffer, selloffer) && !hasBuyOfferHigherPriceThanSellOffer(buyoffer, selloffer)) {
            completeSell(selloffer);
            completeBuy(buyoffer, selloffer.getPrice());
        } else if (hasBuyOfferLessQuantityThanSellOfferQuantity(buyoffer, selloffer) && !hasBuyOfferHigherPriceThanSellOffer(buyoffer, selloffer)) {
            completeBuy(buyoffer, selloffer.getPrice());
            partSell(selloffer, buyoffer.getQuantity());
        } else if (!hasBuyOfferLessQuantityThanSellOfferQuantity(buyoffer, selloffer) && !hasBuyOfferHigherPriceThanSellOffer(buyoffer, selloffer)) {
            partBuy(buyoffer, selloffer.getPrice(), selloffer.getQuantity());
            completeSell(selloffer);
        } else if (hasBuyOfferLessOrEqualQuantityThanOrphanStockQuantity(buyoffer) && isExistingOrphanStockWhichMatches(buyoffer)) {
            stock.setFree_quantity(stock.getFree_quantity() - buyoffer.getQuantity());
            jpacoreservicedao.update(stock);
            completeBuy(buyoffer, buyoffer.getStock().getCurrvalue());
        }
    }

    private boolean hasBuyOfferLessOrEqualQuantityThanOrphanStockQuantity(BuyOffer buyoffer) {
        return buyoffer.getQuantity() <= buyoffer.getStock().getFree_quantity();
    }

    private boolean hasBuyOfferLessQuantityThanSellOfferQuantity(BuyOffer buyoffer, SellOffer selloffer) {
        return buyoffer.getQuantity() < selloffer.getQuantity();
    }

    private boolean haveEqualQauntity(BuyOffer buyoffer, SellOffer selloffer) {
        return buyoffer.getQuantity() == selloffer.getQuantity();
    }

    @Transactional
    private void completeBuy(BuyOffer offer, BigDecimal price) throws CoreServiceException {
        CoreServiceValidator.validateBuyOffer(offer);
        User user = setUserWithNewValues(offer, price);
        createNewLog(user, "vétel", price, offer.getStock().getName(), offer.getQuantity());
        jpacoreservicedao.update(user);
        jpacoreservicedao.delete(offer);
    }

    private User setUserWithNewValues(BuyOffer offer, BigDecimal price) {
        User user = offer.getUser();
        user.getBuyoffers().remove(offer);
        Integer newQuantity = calculateUserStockQuantity(offer, user);
        user.getOwnedstocks().put(offer.getStock(), newQuantity);
        user.setMoney(user.getMoney().subtract(price.multiply(new BigDecimal(offer.getQuantity()))));
        user.setAlloffersvalue(user.getAllbuyoffersvalue().subtract(price.multiply(new BigDecimal(offer.getQuantity()))));
        return user;
    }

    private Integer calculateUserStockQuantity(BuyOffer offer, User user) {
        Integer newQuantity;
        if (hasUserSuchStock(offer, user)) {
            newQuantity = user.getOwnedstocks().get(offer.getStock()) + offer.getQuantity();
        } else {
            newQuantity = offer.getQuantity();
        }
        return newQuantity;
    }

    private boolean hasUserSuchStock(BuyOffer offer, User user) {
        return user.getOwnedstocks() != null && user.getOwnedstocks().containsKey(offer.getStock());
    }

    @Transactional
    private void completeSell(SellOffer soffer) throws CoreServiceException {
        CoreServiceValidator.sellOfferVerification(soffer);
        User user = setUserWithNewValues(soffer);
        jpacoreservicedao.update(user);
        jpacoreservicedao.delete(soffer);
    }

    private User setUserWithNewValues(SellOffer soffer) {
        User user = soffer.getUser();
        user.getSelloffers().remove(soffer);
        user.setMoney(user.getMoney().add(soffer.getPrice().multiply(new BigDecimal(soffer.getQuantity()))));
        createNewLog(user, "eladás", soffer.getPrice(), soffer.getStock().getName(), soffer.getQuantity());
        return user;
    }

    @Transactional
    private void partBuy(BuyOffer boffer, BigDecimal price, int quantity) {
        CoreServiceValidator.validateBuyOffer(boffer);
        User user = setUserWithNewValuesAfterPartBuy(boffer, price, quantity);
        createNewLog(user, "rész-vétel", price, boffer.getStock().getName(), quantity);
        boffer.setQuantity(boffer.getQuantity() - quantity);
        jpacoreservicedao.update(user);
        jpacoreservicedao.update(boffer);
    }

    private User setUserWithNewValuesAfterPartBuy(BuyOffer boffer, BigDecimal price, int quantity) {
        User user = boffer.getUser();
        Integer newQuantity = calculateUserStockQuantityAfterPartBuy(boffer, quantity, user);
        user.getOwnedstocks().put(boffer.getStock(), newQuantity);
        user.setMoney(user.getMoney().subtract(price.multiply(new BigDecimal(quantity))));
        user.setAlloffersvalue(user.getAllbuyoffersvalue().subtract(price.multiply(new BigDecimal(quantity))));
        return user;
    }

    private Integer calculateUserStockQuantityAfterPartBuy(BuyOffer boffer, int quantity, User user) {
        Integer newQuantity;
        if (hasUserSuchStock(boffer, user)) {
            newQuantity = user.getOwnedstocks().get(boffer.getStock()) + quantity;
        } else {
            newQuantity = quantity;
        }
        return newQuantity;
    }

    @Transactional
    private void partSell(SellOffer soffer, int quantity) {
        CoreServiceValidator.sellOfferVerification(soffer);
        User user = setUserWithNewValuesAfterPartSell(soffer, quantity);
        createNewLog(user, "rész-eladás", soffer.getPrice(), soffer.getStock().getName(), quantity);
        soffer.setQuantity(soffer.getQuantity() - quantity);
        jpacoreservicedao.update(user);
        jpacoreservicedao.update(soffer);
    }

    private User setUserWithNewValuesAfterPartSell(SellOffer soffer, int quantity) {
        User user = soffer.getUser();
        user.setMoney(user.getMoney().add(soffer.getPrice().multiply(new BigDecimal(quantity))));
        return user;
    }

    private void createNewLog(User user, String transactionType, BigDecimal price, String stockName, int quantity) {
        Log log = new Log();
        log.setPrice(price);
        log.setQuantity(quantity);
        log.setStock_name(stockName);
        log.setTime(Calendar.getInstance().getTime());
        log.setTransaction_type(transactionType);
        log.setUser(user);
        jpacoreservicedao.save(log);
        user.getLog().add(log);
        jpacoreservicedao.update(user);
    }

    @Transactional
    @Override
    public void modifyStockCurrValue() {
        List<Stock> stocks = findAllStock();
        for (Stock s : stocks) {
            List<Chart> chart = jpacoreservicedao.findChartByStock(s);
            BigDecimal latestPrice = chart.get(0).getClose();
            s.setCurrvalue(latestPrice);
            jpacoreservicedao.save(s);
            updateOffersActiveStatus(s);
        }
    }

    @Transactional
    private void updateOffersActiveStatus(Stock stock) throws CoreServiceException {
        StockMarket stockmarket = jpacoreservicedao.findStockMarketByStock(stock);
        CoreServiceValidator.validateStockMarket(stockmarket);
        if (stockmarket.getBuyers() != null && stockmarket.getSellers() != null) {
            updateBuyOffers(stockmarket.getBuyers());
            updateSellOffers(stockmarket.getSellers());
        }
    }

    private void updateSellOffers(Set<SellOffer> sellers) {
        for (SellOffer sellofferit : sellers) {
            sellofferit.setActive(isActiveSellOffer(sellofferit));
            jpacoreservicedao.update(sellofferit);
        }
    }

    private boolean isActiveSellOffer(SellOffer sellofferit) {
        return sellofferit.getActivationprice().signum() == 0 || sellofferit.getStock().getCurrvalue().compareTo(sellofferit.getActivationprice()) <= 0;
    }

    private void updateBuyOffers(Set<BuyOffer> buyers) {
        for (BuyOffer buyofferit : buyers) {
            buyofferit.setActive(isActiveBuyOffer(buyofferit));
            jpacoreservicedao.update(buyofferit);
        }
    }

    private boolean isActiveBuyOffer(BuyOffer buyofferit) {
        return buyofferit.getActivationprice().signum() == 0 || buyofferit.getStock().getCurrvalue().compareTo(buyofferit.getActivationprice()) >= 0;
    }

    @Override
    public List<Stock> findAllStock() {
        List<Stock> allStock = new ArrayList<>();
        List<StockMarket> allStockmarket = jpacoreservicedao.findAll(StockMarket.class);
        for (StockMarket stockmarket : allStockmarket) {
            allStock.addAll(stockmarket.getStocks());
        }
        return allStock;
    }

    @Override
    public String getLastDate(Stock stock) {
        return jpacoreservicedao.findLastChartDateByStock(stock);
    }

    @Transactional
    @Override
    public void addNewChart(String line, Stock stock) {
        String[] data = line.split("\t");
        createNewChart(data, stock);
    }

    @Transactional
    private void createNewChart(String[] data, Stock stock) {
        Chart chart = new Chart();
        Calendar actTime = Calendar.getInstance();
        String[] date = data[0].split("-");
        actTime.set(Integer.parseInt(date[0]), Integer.parseInt(date[1]) - 1, Integer.parseInt(date[2]));
        stock = jpacoreservicedao.findById(Stock.class, stock.getName());
        if (stock.getChart() != null && !stock.getChart().isEmpty()) {
            if (stock.getChart().get(0).getTime().before(actTime.getTime())) {
                initAndSaveChart(chart, data, actTime, stock);
            }
        } else {
            initAndSaveChart(chart, data, actTime, stock);
        }

    }

    private void initAndSaveChart(Chart chart, String[] data, Calendar actTime, Stock stock) {
        chart.setTime(actTime.getTime());
        chart.setOpen(new BigDecimal(data[1]));
        chart.setClose(new BigDecimal(data[2]));
        chart.setMin(new BigDecimal(data[3]));
        chart.setMax(new BigDecimal(data[4]));
        chart.setStock(stock);
        jpacoreservicedao.save(chart);
    }

    @Transactional
    @Override
    public void addNewStockMarket(String stockmarketName, int open, int close) {
        createNewStockMarket(stockmarketName, open, close);
    }

    @Transactional
    private void createNewStockMarket(String stockmarketName, int open, int close) {
        if (jpacoreservicedao.findById(StockMarket.class, stockmarketName) == null) {
            StockMarket stockmarket = new StockMarket();
            stockmarket.setName(stockmarketName);
            stockmarket.setOpeninghour(open);
            stockmarket.setClosinghour(close);
            jpacoreservicedao.save(stockmarket);
        }
    }

    @Transactional
    @Override
    public void addNewStock(String stockmarketName, String stockName, String companyName, int freeQauntity) {
        Stock stock;
        if ((stock = createStock(stockName, companyName, freeQauntity)) != null) {
            StockMarket sm = jpacoreservicedao.findById(StockMarket.class, stockmarketName);
            Set<Stock> stocks = sm.getStocks() == null ? new HashSet<>() : sm.getStocks();
            stocks.add(stock);
            sm.setStocks(stocks);
            jpacoreservicedao.update(sm);
        }

    }

    @Transactional
    private Stock createStock(String stockName, String companyName, int freeQauntity) {
        if (jpacoreservicedao.findById(Stock.class, stockName) == null) {
            Stock stock = new Stock();
            stock.setName(stockName);
            stock.setCname(companyName);
            stock.setFree_quantity(freeQauntity);
            jpacoreservicedao.save(stock);
            return stock;
        }
        return null;
    }

}
