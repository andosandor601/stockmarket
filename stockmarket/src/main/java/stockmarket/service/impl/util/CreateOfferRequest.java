package stockmarket.service.impl.util;

import java.math.BigDecimal;

import stockmarket.domain.entities.Stock;
import stockmarket.domain.entities.User;

public class CreateOfferRequest {
    private User user;
    private int quantity;
    private BigDecimal price;
    private Stock stock;
    private BigDecimal activationPrice;

    public CreateOfferRequest(User user, int quantity, BigDecimal price, Stock stock, BigDecimal activationPrice) {
        setUser(user);
        setQuantity(quantity);
        setPrice(price);
        setStock(stock);
        setActivationprice(activationPrice);
    }

    public CreateOfferRequest(User user, int quantity, BigDecimal price, Stock stock) {
        setUser(user);
        setQuantity(quantity);
        setPrice(price);
        setStock(stock);
        setActivationprice(BigDecimal.ZERO);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public BigDecimal getActivationprice() {
        return activationPrice;
    }

    public void setActivationprice(BigDecimal activationprice) {
        this.activationPrice = activationprice;
    }
}
