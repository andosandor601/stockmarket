package stockmarket.domain.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class SellOffer {

    @Id
    @GeneratedValue
    private long sellid;

    @OneToOne
    private User users;

    @OneToOne
    private StockMarket stockmarket;
    private int quantity;

    @Column(precision = 30, scale = 10)
    private BigDecimal price;

    @ManyToOne
    @JoinColumn
    private Stock stock;

    @Column(precision = 30, scale = 10)
    private BigDecimal activationprice;

    private boolean isActive;

    public long getSellid() {
        return sellid;
    }

    public User getUser() {
        return users;
    }

    public void setUser(User user) {
        this.users = user;
    }

    public StockMarket getStockmarket() {
        return stockmarket;
    }

    public void setStockmarket(StockMarket stockmarket) {
        this.stockmarket = stockmarket;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public BigDecimal getActivationprice() {
        return activationprice;
    }

    public void setActivationprice(BigDecimal activationprice) {
        this.activationprice = activationprice;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "SellOffer [sellid=" + sellid + ",\n user=" + users + ", quantity=" + quantity + ", price=" + price + ",\n stock=" + stock + ", activationprice="
                + activationprice + "]";
    }

}
