package stockmarket.domain.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "BuyOffer")
public class BuyOffer {

    @Id
    @GeneratedValue
    private long buyid;

    @OneToOne
    private User userb;

    @OneToOne
    private StockMarket stockmarket;

    private int quantity;

    @Column(precision = 30, scale = 10)
    private BigDecimal price;

    @ManyToOne
    private Stock stock;

    @Column(precision = 30, scale = 10)
    private BigDecimal activationprice;

    private boolean isActive;

    public long getBuyid() {
        return buyid;
    }

    public User getUser() {
        return userb;
    }

    public void setUser(User user) {
        this.userb = user;
    }

    public StockMarket getStockmarket() {
        return stockmarket;
    }

    public void setStockmarket(StockMarket stockmarket) {
        this.stockmarket = stockmarket;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public BigDecimal getActivationprice() {
        return activationprice;
    }

    public void setActivationprice(BigDecimal activationprice) {
        this.activationprice = activationprice;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "BuyOffer [buyid=" + buyid + ",\n user=" + userb + ",\n quantity=" + quantity + ", price=" + price + ", \n stock=" + stock
                + ",\n activationprice=" + activationprice + "]";
    }

}
