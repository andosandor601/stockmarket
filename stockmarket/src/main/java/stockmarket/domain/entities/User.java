package stockmarket.domain.entities;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "usr")
public class User {

    private String email;
    private String password;

    @Id
    private String name;
    private String bank_account;

    @Column(precision = 30, scale = 10)
    private BigDecimal money;

    @OneToMany(mappedBy = "userb", targetEntity = BuyOffer.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<BuyOffer> buy_offers;

    @OneToMany(mappedBy = "users", targetEntity = SellOffer.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<SellOffer> sell_offers;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "User_Owned_Stocks")
    @MapKeyColumn(name = "ownedstocksmapkeycolumn")
    private Map<Stock, Integer> ownedstocks;

    @Column(precision = 30, scale = 10)
    private BigDecimal all_buyoffers_value;

    @OneToMany(mappedBy = "user", targetEntity = Log.class, fetch = FetchType.EAGER)
    @OrderBy("time DESC")
    private List<Log> log;

    public List<Log> getLog() {
        return log;
    }

    public void setLog(List<Log> log) {
        this.log = log;
    }

    public Set<SellOffer> getSelloffers() {
        return sell_offers;
    }

    public void setSelloffers(Set<SellOffer> selloffers) {
        this.sell_offers = selloffers;
    }

    public Set<BuyOffer> getBuyoffers() {
        return buy_offers;
    }

    public void setBuyoffers(Set<BuyOffer> buyoffers) {
        this.buy_offers = buyoffers;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBankaccount() {
        return bank_account;
    }

    public void setBankaccount(String bankaccount) {
        this.bank_account = bankaccount;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getAllbuyoffersvalue() {
        return all_buyoffers_value;
    }

    public void setAlloffersvalue(BigDecimal allbuyoffersvalue) {
        this.all_buyoffers_value = allbuyoffersvalue;
    }

    public Map<Stock, Integer> getOwnedstocks() {
        return ownedstocks;
    }

    public void setOwnedstocks(Map<Stock, Integer> ownedstocks) {
        this.ownedstocks = ownedstocks;
    }

    @Override
    public String toString() {
        return "User [name=" + name + ", email=" + email + ", bankaccount=" + bank_account + ", money=" + money + ", allbuyoffersvalue=" + all_buyoffers_value
                + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

}
