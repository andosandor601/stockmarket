package stockmarket.domain.entities;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "name", "cname" }) })
public class Stock {

    @Id
    private String name;
    private String cname;
    private int free_quantity;

    @Column(precision = 30, scale = 10)
    private BigDecimal currvalue;

    @OneToMany(mappedBy = "stock", targetEntity = Chart.class, fetch = FetchType.EAGER)
    @OrderBy("time DESC")
    private List<Chart> chart;

    public int getFree_quantity() {
        return free_quantity;
    }

    public void setFree_quantity(int free_quantity) {
        this.free_quantity = free_quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public BigDecimal getCurrvalue() {
        return chart.get(0).getClose();
    }

    public void setCurrvalue(BigDecimal currvalue) {
        this.currvalue = currvalue;
    }

    @Override
    public String toString() {
        return "Stock [\n  name=" + name + ", cname=" + cname + ", currvalue=" + currvalue + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cname == null) ? 0 : cname.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Stock other = (Stock) obj;
        if (cname == null) {
            if (other.cname != null)
                return false;
        } else if (!cname.equals(other.cname))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    public List<Chart> getChart() {
        return chart;
    }

    public void setChart(List<Chart> chart) {
        this.chart = chart;
    }

}
