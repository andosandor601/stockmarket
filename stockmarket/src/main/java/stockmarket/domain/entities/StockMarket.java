package stockmarket.domain.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class StockMarket {

    @Id
    private String name;
    private int openinghour;
    private int closinghour;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<Stock> stocks;

    @OneToMany(mappedBy = "stockmarket", targetEntity = BuyOffer.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @OrderBy("stock, price DESC")
    private Set<BuyOffer> buyers;

    @OneToMany(mappedBy = "stockmarket", targetEntity = SellOffer.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @OrderBy("stock, price ASC")
    private Set<SellOffer> sellers;

    public int getOpeninghour() {
        return openinghour;
    }

    public void setOpeninghour(int openinghour) {
        this.openinghour = openinghour;
    }

    public int getClosinghour() {
        return closinghour;
    }

    public void setClosinghour(int closinghour) {
        this.closinghour = closinghour;
    }

    public Set<Stock> getStocks() {
        return stocks;
    }

    public void setStocks(Set<Stock> stocks) {
        this.stocks = stocks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "StockMarket [name=" + name + ", openinghour=" + openinghour + ", closinghour=" + closinghour + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((buyers == null) ? 0 : buyers.hashCode());
        result = prime * result + closinghour;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + openinghour;
        result = prime * result + ((sellers == null) ? 0 : sellers.hashCode());
        result = prime * result + ((stocks == null) ? 0 : stocks.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StockMarket other = (StockMarket) obj;
        if (buyers == null) {
            if (other.buyers != null)
                return false;
        } else if (!buyers.equals(other.buyers))
            return false;
        if (closinghour != other.closinghour)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (openinghour != other.openinghour)
            return false;
        if (sellers == null) {
            if (other.sellers != null)
                return false;
        } else if (!sellers.equals(other.sellers))
            return false;
        if (stocks == null) {
            if (other.stocks != null)
                return false;
        } else if (!stocks.equals(other.stocks))
            return false;
        return true;
    }

    public Set<BuyOffer> getBuyers() {
        return buyers;
    }

    public void setBuyers(Set<BuyOffer> buyers) {
        this.buyers = buyers;
    }

    public Set<SellOffer> getSellers() {
        return sellers;
    }

    public void setSellers(Set<SellOffer> sellers) {
        this.sellers = sellers;
    }

}
