package stockmarket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import stockmarket.dao.impl.JpaCoreServiceDaoImpl;
import stockmarket.dao.impl.JpaUserServiceDaoImpl;
import stockmarket.dao.interfaces.JpaCoreServiceDao;
import stockmarket.dao.interfaces.JpaUserServiceDao;
import stockmarket.service.background.InitDatabase;
import stockmarket.service.impl.CoreServiceImpl;
import stockmarket.service.impl.UserServiceImpl;
import stockmarket.service.interfaces.CoreService;
import stockmarket.service.interfaces.UserService;

@Configuration
@EnableJpaRepositories
public class StockMarketConfig {

    @Bean
    public JpaCoreServiceDao jpaCoreServiceDao() {
        return new JpaCoreServiceDaoImpl();
    }

    @Bean
    public JpaUserServiceDao jpaUserServiceDao() {
        return new JpaUserServiceDaoImpl();
    }

    @Bean
    public UserService userService(JpaUserServiceDao jpauserservicedao) {
        return new UserServiceImpl(jpauserservicedao);
    }

    @Bean
    public CoreService coreservice(JpaCoreServiceDao jpacoreservicedao) {
        return new CoreServiceImpl(jpacoreservicedao);
    }

    @Bean
    public InitDatabase updateDatabase() {
        return new InitDatabase();
    }
}
