package stockmarket.presenter;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import stockmarket.domain.entities.BuyOffer;
import stockmarket.domain.entities.Chart;
import stockmarket.domain.entities.SellOffer;
import stockmarket.domain.entities.Stock;
import stockmarket.domain.entities.User;
import stockmarket.presenter.util.ChartDataRepresenter;
import stockmarket.service.background.InitDatabase;
import stockmarket.service.exceptions.UserServiceException;
import stockmarket.service.impl.util.CreateOfferRequest;
import stockmarket.service.interfaces.CoreService;
import stockmarket.service.interfaces.UserService;
import stockmarket.web.enums.ChartInterval;

@Service
public class StockTradingPresenter {

    @Autowired
    private UserService userService;

    @Autowired
    private CoreService coreService;

    public User doAddMoney(User user, BigDecimal value) throws UserServiceException {
        return userService.depositMoney(user, value);
    }

    public User doTransferMoney(User user, BigDecimal value) throws UserServiceException {
        return userService.withdrawMoney(user, value);
    }

    public List<ChartDataRepresenter> doLoadChart(String stockData, ChartInterval interval) throws UserServiceException {
        String[] stockDataParts = stockData.trim().split("-");
        Stock stock = findStock(stockDataParts[0], stockDataParts[1]);
        List<Chart> chart = stock.getChart();
        return interval == ChartInterval.DAY ? loadDayChart(chart) : (interval == ChartInterval.WEEK ? loadWeekChart(chart) : loadMonthChart(chart));
    }
    
    public Stock findStock(String stockName, String companyName) {
        return userService.findStock(stockName, companyName);
    }

    private List<ChartDataRepresenter> loadMonthChart(List<Chart> chart) {
        List<ChartDataRepresenter> cdrepList = new ArrayList<>();
        ChartDataRepresenter cdrep = new ChartDataRepresenter();
        for (Chart cit : chart) {
            if (cdrep.getDate() == null) {
                setChartDataRepresenterMonthly(cdrep, cit);
            } else if (cdrep.getYear() == (cit.getTime().getYear() + 1900) && cdrep.getMonth() == (cit.getTime().getMonth() + 1)) {
                cdrep.addChart(cit);
            } else {
                cdrepList.add(cdrep);
                cdrep = new ChartDataRepresenter();
                setChartDataRepresenterMonthly(cdrep, cit);
            }
        }
        cdrepList.add(cdrep);
        return cdrepList;
    }

    private void setChartDataRepresenterMonthly(ChartDataRepresenter cdrep, Chart cit) {
        int year = cit.getTime().getYear() + 1900;
        int month = cit.getTime().getMonth() + 1;
        cdrep.setYear(year);
        cdrep.setMonth(month);
        cdrep.setDate(year + "-" + month + "-01");
        cdrep.addChart(cit);
    }

    private List<ChartDataRepresenter> loadWeekChart(List<Chart> chart) {
        List<ChartDataRepresenter> cdrepList = new ArrayList<>();
        ChartDataRepresenter cdrep = new ChartDataRepresenter();
        Calendar cal = Calendar.getInstance();
        for (Chart cit : chart) {
            cal.setTime(cit.getTime());
            if (cdrep.getDate() == null) {
                setChartDataRepresenterWeekly(cdrep, cit);
            } else if (cdrep.getYear() == cit.getTime().getYear() + 1900 && cdrep.getWeek() == cal.get(Calendar.WEEK_OF_YEAR)) {
                cdrep.addChart(cit);
            } else {
                cdrepList.add(cdrep);
                cdrep = new ChartDataRepresenter();
                setChartDataRepresenterWeekly(cdrep, cit);
            }
        }
        cdrepList.add(cdrep);
        return cdrepList;
    }

    private void setChartDataRepresenterWeekly(ChartDataRepresenter cdrep, Chart cit) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(cit.getTime());
        int year = cit.getTime().getYear() + 1900;
        int week = cal.get(Calendar.WEEK_OF_YEAR);
        int month = cit.getTime().getMonth() + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        cdrep.setYear(year);
        cdrep.setMonth(month);
        cdrep.setWeek(week);
        cdrep.setDate(year + "-" + month + "-" + day);
        cdrep.addChart(cit);
    }

    private List<ChartDataRepresenter> loadDayChart(List<Chart> chart) {
        List<ChartDataRepresenter> cdrepList = new ArrayList<>();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (Chart cit : chart) {
            cdrepList.add(new ChartDataRepresenter(dateFormat.format(cit.getTime()), cit.getOpen().doubleValue(), cit.getMax().doubleValue(),
                    cit.getMin().doubleValue(), cit.getClose().doubleValue()));
        }
        return cdrepList;
    }

    public BuyOffer doNewBuyOffer(User user, int quantity, BigDecimal price, String stockData) throws UserServiceException {
        String[] stockDataParts = stockData.trim().split("-");
        Stock stock = userService.findStock(stockDataParts[0], stockDataParts[1]);
        CreateOfferRequest offerRequest = new CreateOfferRequest(user, quantity, price, stock);
        BuyOffer buyoffer = userService.newBuyOffer(offerRequest);
        return buyoffer;
    }

    public SellOffer doNewSellOffer(User user, int quantity, BigDecimal price, String stockData) throws UserServiceException {
        String[] stockDataParts = stockData.trim().split("-");
        Stock stock = userService.findStock(stockDataParts[0], stockDataParts[1]);
        CreateOfferRequest offerRequest = new CreateOfferRequest(user, quantity, price, stock);
        SellOffer selloffer = userService.newSellOffer(offerRequest);
        return selloffer;
    }

    public BuyOffer doNewStopBuyOffer(User user, int quantity, BigDecimal price, String stockData, BigDecimal activationPrice) throws UserServiceException {
        String[] stockDataParts = stockData.trim().split("-");
        Stock stock = userService.findStock(stockDataParts[0], stockDataParts[1]);
        CreateOfferRequest offerRequest = new CreateOfferRequest(user, quantity, price, stock, activationPrice);
        BuyOffer buyoffer = userService.newStopBuyOffer(offerRequest);
        return buyoffer;
    }

    public SellOffer doNewStopSellOffer(User user, int quantity, BigDecimal price, String stockData, BigDecimal activationprice) throws UserServiceException {
        String[] stockDataParts = stockData.trim().split("-");
        Stock stock = userService.findStock(stockDataParts[0], stockDataParts[1]);
        CreateOfferRequest offerRequest = new CreateOfferRequest(user, quantity, price, stock, activationprice);
        SellOffer selloffer = userService.newStopSellOffer(offerRequest);
        return selloffer;
    }

    public void doDeleteBuyOffer(long buyofferId) throws UserServiceException {
        userService.deleteBuyOffer(buyofferId);
    }

    public void doDeleteSellOffer(long sellofferId) throws UserServiceException {
        userService.deleteSellOffer(sellofferId);
    }

    public BuyOffer doModifyBuyOfferPrice(long buyofferId, BigDecimal newPrice) throws UserServiceException {
        BuyOffer buyoffer = userService.modifyBuyOfferPrice(buyofferId, newPrice);
        return buyoffer;
    }

    public SellOffer doModifySellOfferPrice(long sellofferId, BigDecimal newPrice) throws UserServiceException {
        SellOffer selloffer = userService.modifySellOfferPrice(sellofferId, newPrice);
        return selloffer;
    }

    public BuyOffer doModifyBuyOfferQuantity(long buyofferId, int newQuantity) throws UserServiceException {
        BuyOffer buyoffer = userService.modifyBuyOfferQuantity(buyofferId, newQuantity);
        return buyoffer;
    }

    public SellOffer doModifySellOfferQuantity(long sellofferId, int newQuantity) throws UserServiceException {
        SellOffer selloffer = userService.modifySellOfferQuantity(sellofferId, newQuantity);
        return selloffer;
    }

    public BuyOffer doModifyBuyOfferActivationPrice(long buyofferId, BigDecimal newActivationPrice) throws UserServiceException {
        BuyOffer buyoffer = userService.modifyBuyOfferActivationPrice(buyofferId, newActivationPrice);
        return buyoffer;
    }

    public SellOffer doModifySellOfferActivationPrice(long sellofferId, BigDecimal newActivationPrice) throws UserServiceException {
        SellOffer selloffer = userService.modifySellOfferActivationPrice(sellofferId, newActivationPrice);
        return selloffer;
    }

    public List<Stock> doListAllStock() throws UserServiceException {
        return userService.getAllStock();
    }

    public Stock doFindStock(String stockName, String companyName) throws UserServiceException {
        return userService.findStock(stockName, companyName);
    }

    public User doFindUser(String userName) throws UserServiceException {
        return userService.findUserByName(userName);
    }

    public BuyOffer doFindBuyOffer(long buyofferId) throws UserServiceException {
        return userService.findBuyOfferById(buyofferId);
    }

    public SellOffer doFindSellOffer(long sellofferId) throws UserServiceException {
        return userService.findSellOfferById(sellofferId);
    }

    // CoreService

    /** 15 percenként 6-20-ig hétköznap */
    @Scheduled(cron = "0 0/15 6-20 * * MON-FRI")
    private void makeTransactions() {
        try {
            coreService.findTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "0 0 6 * * MON-FRI")
    private void updateDatabase() {
        new InitDatabase().updateStocks();
    }

}
