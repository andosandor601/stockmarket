package stockmarket.presenter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stockmarket.domain.entities.User;
import stockmarket.service.exceptions.UserServiceException;
import stockmarket.service.interfaces.UserService;

@Service
public class UserPresenter {

    @Autowired
    private UserService userService;

    public User doLogin(String userName, String password) throws UserServiceException {
        return userService.login(userName, password);
    }

    public User doSignup(String userName, String password, String email, String bankAccount) throws UserServiceException {
        return userService.signup(userName, password, email, bankAccount);
    }

}
