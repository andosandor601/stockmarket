package stockmarket.presenter.util;

import stockmarket.domain.entities.Chart;

public class ChartDataRepresenter {

    private String date;
    private int year;
    private int month;
    private int week;
    private double open;
    private double max;
    private double min;
    private double close;

    public ChartDataRepresenter() {
        super();
    }

    public ChartDataRepresenter(String date, double open, double max, double min, double close) {
        super();
        this.date = date;
        this.open = open;
        this.max = max;
        this.min = min;
        this.close = close;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public void addChart(Chart chart) {
        close = chart.getClose().doubleValue();
        if (open == 0.0d) {
            open = close;
            max = close;
            min = close;
        } else if (close > max) {
            max = close;
        } else if (close < min) {
            min = close;
        }
    }

    @Override
    public String toString() {
        return "ChartDataRepresenter [date=" + date + ", open=" + open + ", max=" + max + ", min=" + min + ", close=" + close + "]\n";
    }

}
