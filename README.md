# README #

Tőzsdei kereskedőház leírása

### Feladatleírás ###

A feladat egy olyan tőzsdei kereskedőház létrehozása, amelyben értelemszerűen a a felhasználók kereskedhetnek különböző értékpapírokkal. A kereskedés mellett a program automatikus grafikonelemzési szolgáltatást nyújt.

### Részletes követelményjegyzék ###

**Felhasználó: **

* Az újonnan érkezőknek lehetőségük van regisztrálni, ekkor 0 lesz a számlájukon a keretösszeg
* A felhasználók be tudjanak lépni, a saját fiókjukba.
* A felhasználóknak van vásárlási keretük, és vásárolt értékpapírjuk
* A felhasználók tudják a vásárlási keretüket növelni(pénz feltöltés), és lehetőség van pénz kivételére(átutalás saját számlára)
* A program naplózza a vásárolt részvényeket
* A felhasználók tudjanak keresni a különböző értékpapírok között, amit aztán a program megjelenít grafikon formában
* A grafikonon lehet az időtartamot állítani
* A program egyből elemzést készít, amikor a grafikon betölt, átméretezésnél is(rövid-hosszútávú változások miatt)
* A program elemzés után értesíti a felhasználót az eredményről
* A program lehetőséget biztosít, hogy a kiválasztott értékpapírral kereskedhessen
* A felhasználó a kiválasztott értékpapírra beállíthat automatikus részvényeladást/vételt(stop limit megbízás), ahol megadhatja a vásárolni kívánt mennyiséget, és a vásárlás/eladási ár határát, ahol, ha az árfolyam eléri ezt a szintet a program automatikusan létrehoz egy új vételi, vagy eladási megbízást

**Kereskedés: **

* Kereskedés csak két felhasználó között jöhet létre
* Részvényt csak akkor vehet(a beírt áron), ha valaki a részvényt eladásra kínálta(szintén ezen az áron, ugyanabból az értékpapírból), továbbá az eladás is akkor történik meg, amikor jelentkezik rá vevő
* Az üzlet létrejöttét a program eltárolja az adatbázisban(grafikonon megjelenik), továbbá a vásárlás összegét levonja a vevőtől és jóváírja az eladó számláján 
* Ha az eladó által kínálta mennyiség nagyobb, mint amennyit a vevő vásárolni szeretne, akkor a kisebb mennyiségre köttetik meg az üzlet, a fennmaradó mennyiségre pedig tovább keresi a vevőt. Ugyanígy fordított irányba is
* Vásárolni csak addig lehet, ameddig a felhasználó kerete még pozitív marad
* A vásárlás/eladás sikerességéről a program értesítést küld a felhasználónak
* A zárás és nyitás között nem köttetnek üzletek

**Táblázat **
![tablestockmarket2.png](https://bitbucket.org/repo/LMyyM4/images/3619411562-tablestockmarket2.png)

### Entitás modell ###

![entitymodel12.gif](https://bitbucket.org/repo/LMyyM4/images/1524825021-entitymodel12.gif)

### Felületi tervek ###

** Belépési képernyő **

![loginscreen.jpg](https://bitbucket.org/repo/LMyyM4/images/3323497919-loginscreen.jpg)

** Regisztrációs képernyő **

![signupscreen.jpg](https://bitbucket.org/repo/LMyyM4/images/204765785-signupscreen.jpg)

** Adatlap **

![User-form.jpg](https://bitbucket.org/repo/LMyyM4/images/1585646191-User-form.jpg)

** Részvény vásárlás **

![buystockscreen.jpg](https://bitbucket.org/repo/LMyyM4/images/4270501437-buystockscreen.jpg)

** Saját részvények megtekintése **

![OwnedStocksScreen.jpg](https://bitbucket.org/repo/LMyyM4/images/3395821406-OwnedStocksScreen.jpg)